# Exercice 2

# question 1. et 2.
G = [
    [1000, 10, 3, 1000, 6, 1000],
    [0, 1000, 1000, 1000, 1000, 1],
    [1000, 4, 1000, 1000, 2, 1000],
    [1000, 1000, 1, 1000, 3, 1000],
    [1000, 0, 1000, 1000, 1000, 1],
    [2, 1000, 1000, 1000, 1000, 1000],
]


# question 3.
def poidsMax(g):
    infini = g[0][0]
    test = False
    for i in range(len(g)):
        for j in range(len(g)):
            if g[i][j] != infini:
                if test is False:
                    max = g[i][j]
                    test = True
                else:
                    if g[i][j] > max:
                        max = g[i][j]
    return max


# print("la cout maximal est: ", poidsMax(G))


# question 4.
def sommetAcc(g, s):
    infini = g[0][0]
    t = ()
    s2 = s - 1
    for j in range(len(g)):
        if g[s2][j] != infini:
            x = j + 1
            t += (x,)
    return t


# print("les sommets accessibles depuis le sommet 1: ", sommetAcc(G, 1))


# question 5.
def degre(g, s):
    d = 0
    s2 = s - 1
    infini = g[0][0]
    for i in range(len(g)):
        if g[s2][i] != infini:
            d += 1
        if g[i][s2] != infini:
            d += 1
    return d


# print("le degre du sommet 1: ", degre(G, 1))


# question 6.
def degreMax(g):
    max = degre(g, 1)
    for i in range(2, len(g) + 1):
        x = degre(g, i)
        if x > max:
            max = x
    return max


# print("le degre maximal est: ", degreMax(G))


# question 7.
def regulier(g):
    d = degre(g, 1)
    for i in range(2, len(g) + 1):
        if degre(g, i) != d:
            return False
    return True


# print("le graphe est regulier: ", regulier(G))


# question 8.
def longueur(g, liste):
    d = 0
    if len(liste) <= 1:
        return -1
    infini = g[0][0]
    for i in range(len(liste) - 1):
        x = liste[i] - 1
        y = liste[1 + i] - 1
        if g[x][y] == infini:
            return -1
        d = d + g[x][y]
    return d


# print(longueur(G, [1, 3, 5, 6]))
