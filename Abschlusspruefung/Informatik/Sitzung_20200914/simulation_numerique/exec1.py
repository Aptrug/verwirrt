import numpy

import pylab

x = numpy.linspace(0, 10 * numpy.pi, 200)
y = numpy.cos(x)
pylab.plot(x, y)
y = numpy.exp(-x / 10) * numpy.cos(x)
pylab.plot(x, y)
pylab.xlabel('x')
pylab.ylabel('y = f(x)')
pylab.title('Titre de votre choix')
pylab.show()
