# Exercice 4

soit $f(n)$ le nombre d'itérations élémentaires

## a)

$$
f(n) = 2n^2 = O(n^2)
$$

## b)

$$
f(n) = n(n-1) = O(n^2)
$$

## c)

### meine Antwort

Take a look at big_o.py

### ihre Antwort

On a deux boucle imbriquées: la première boucle contient $log_2(n)$ itération;
la deuxième boucle contiendra $n$ itération alors la complexité est d'ordre
$O(nlog_2(n))$
