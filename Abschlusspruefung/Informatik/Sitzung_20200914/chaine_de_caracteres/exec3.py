# competer les nombre de caractères par type (majuscule, miniscule ...) dans
# un string


def nbreMajMinAutre(ch):
    ma = 0
    mi = 0
    a = 0
    for x in ch:
        if x >= 'A' and x <= 'Z':
            ma += 1
        elif x >= 'a' and x <= 'z':
            mi += 1
        else:
            a += 1
    return (ma, mi, a)


ch = 'Bonjour Tout Le Monde!!!'

question = float(input('Question: '))
if question == 1.:
    print(nbreMajMinAutre(ch))
elif question == 2.:
    print('Le nombre des majuscules: ', nbreMajMinAutre(ch)[0])
    print('Le nombre des miniscules: ', nbreMajMinAutre(ch)[1])
    print('Le nombre des autres caractères: ', nbreMajMinAutre(ch)[2])
