def transMat(A):
    C = [[0 for i in range(len(A))] for j in range(len(A[0]))]
    for i in range(len(A)):
        for j in range(len(A[0])):
            C[i][j] = A[i][j]
    return C


M = [[43, 24, 32, 43], [24, 32, 42, 14], [32, 52, 74, 82], [95, 29, 84, 72]]
C = transMat(M)
print(C)
