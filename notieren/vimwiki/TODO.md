# Long term goals

## Programming languages

C, Go, Rust, Vimscript, regex, awk, sql (postegreql specifically)
take a look at programming paradigms like OOP (Object-oriented programming)
Learn Docker and then Kubernetes

get familiar with yaml (it is better than json and more supported than
toml, I have done my research)

## Learn Kali

expirement a little bit with wireshark aircrack-ng and other useful tools
check this out too: <https://github.com/may215/awesome-termux-hacking>

## Packaging somes packages

revive (go linter),

## Learn some javascript

practice by making javascript browser user scripts

## Scrape 192.168.1.1

extract some useful information like number of connected devices and internet
speed

## show output of a script (stdout, like printf 'testing') in android status bar

worst case scenario, you make an app for it

## Stream audio from Android to NixOS and vice versa

Use Termux for that
Package roc-toolkit for nixos, it's like Soundwire, but open-source

## Make Android apps

todo list (widget only)
aria2rpc client using termux (it works with Kiwi browser)
show text file contents and maybe modify them ( widget only)

## Play Skyrim

I should have a long time ago
