# Savoir contredire

## aus arasite.org/nietgaysci.html

La capacité à accepter à la fois la contradiction et la critique est le signe
d'un être humain supérieur, d'un esprit libéré, même si peu de gens le
reconnaissent.
