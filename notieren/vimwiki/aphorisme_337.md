# L’humanité à venir

## aus arasite.org/nietgaysci.html

Lorsque nous regardons le présent à travers des perspectives dérivées des
civilisations classiques, une chose qui émerge est la proéminence d'un sens
historique.  Celui-ci a des effets intéressants à peine réalisés, et tout le
monde ne veut pas le développer.  Contempler l'ensemble de l'histoire de
l'humanité nous rendra nostalgiques, tristes ou désillusionnés, mais si nous
parvenons à surmonter ce sentiment, cela pourrait renouveler notre obligation
envers le passé, pour développer ses plus nobles idéaux.  Si cela pouvait être
géré, le bonheur humain se développerait, comme un sentiment divin, basé sur le
pouvoir et l'amour, les larmes et le rire.
