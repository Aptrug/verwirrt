# Aux navires

# aus <https://www.reddit.com/r/Nietzsche/comments/gcugt5/the_gay_science_reevaluation_of_values_1116/>

Nietzsche utilise la métaphore de la rotation autour d'un soleil
pour souligner que les différentes personnes devraient avoir leurs
propres justifications philosophiques pour leurs modes de vie -
ces justifications sont comme le soleil dans le sens où les individus
tournent autour d'eux, ce qui était un thème du numéro 125 car
Dieu était le Soleil autour duquel tout le monde tournait. Nietzsche
soutient qu'il devrait y avoir plus de soleils différents, c'est-à-dire
plus de justifications philosophiques diverses pour la façon dont
on vit. Les gens n'ont pas besoin de pitié ou de pardon pour ne
pas vivre comme les autres. La Terre morale est également ronde
et elle a aussi ses antipodes (opposés) - les opposés ont le droit
d'exister (si la Terre morale était plate, alors il ne pourrait
pas y avoir d'opposés comme le pôle nord et le pôle sud). Nous
devrions réaliser que la Terre morale n'est pas plate, et nous
devrions nous embarquer pour trouver notre propre vision du monde,
notre propre soleil - il fait appel à l'ère de l'exploration où
les gens naviguaient à travers le monde pour découvrir de nouvelles
terres - nous devons réévaluer les valeurs.

## aus arasite.org/nietgaysci.html

Il est courant pour chacun de trouver une justification à sa façon de vivre et
cela le rend heureux, mais ce dont il a réellement besoin, c'est de justice et
de nouveaux philosophes, qui embrasseront également les "antipodes" de la "terre
morale" [vraisemblablement soit des humeurs de dépression noire, soit tous les
vieux trucs sur la célébration de la cruauté et du mal...].
