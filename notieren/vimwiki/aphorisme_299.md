# Ce que l’on doit apprendre des artistes

## aus <https://www.reddit.com/r/Nietzsche/comments/gf5zm5/the_gay_science_nihilism_and_living_as_artists/>

Les choses en elles-mêmes ne sont pas belles, attrayantes ou désirables,
mais nous pouvons apprendre des artistes comment voir les choses
comme étant belles. Les artistes ont des techniques pour voir les
choses comme étant belles : regarder quelque chose de loin ou d'une
certaine perspective, cadrer quelque chose d'une certaine manière,
utiliser un filtre ou un certain type d'éclairage, etc. En fait,
nous pouvons et devons aller au-delà des artistes en appliquant
leurs techniques (de manière métaphorique) à notre propre vie pour
voir notre monde et nous-mêmes comme étant beaux (parce qu'en soi,
la vie n'est ni belle ni laide).

## aus arasite.org/nietgaysci.html

Nous devrions apprendre des artistes comment mettre en évidence et attirer
l'attention sur des aspects particuliers de la réalité, même si nous sommes
"plus sages qu'eux dans d'autres domaines" (240).  Nous devons prolonger la
poésie dans la vie.
