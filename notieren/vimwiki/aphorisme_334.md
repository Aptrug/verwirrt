# On doit apprendre à aimer

## aus arasite.org/nietgaysci.html

Nous devons apprendre à apprécier des choses comme la musique, et apprendre à la
comprendre et à la tolérer, voire à en tomber amoureux et à en devenir
dépendants.  C'est la même chose pour tout le reste : il faut apprendre à aimer.
