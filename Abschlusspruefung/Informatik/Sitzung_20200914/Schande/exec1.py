import numpy

import pylab

import tikzplotlib

x = numpy.linspace(0, 10 * numpy.pi, 200)
y = numpy.cos(x)
pylab.plot(x, y)
y = numpy.exp(-x / 10) * numpy.cos(x)
pylab.plot(x, y)
pylab.xlabel('x')
pylab.ylabel('y = f(x)')
pylab.title('Graphe')
tikzplotlib.save('exec1.tex')
