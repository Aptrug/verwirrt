# Partie 2

## Colle 1

Every epoch, every civilization has what Nietzsche calls its "table of values";
in other words, it admits a hierarchy of values; it considers one thing
superior to another; it believes that one action is preferable to another; it
judges, to take a particular example, that truth is superior to error or that a
merciful act is preferable to an act of cruelty. The determination of this
table of values, and in particular the setting of the highest values, is the
capital fact of universal history, since this hierarchy of values determines
the conscious or unconscious acts of all individuals and motivates all the
judgments we make about these acts. For the philosopher, this problem of the
determination of values thus takes precedence over all others; it is on it, in
any case, that Nietzsche concentrated all his efforts. And the result of his
meditations was the following: the table of values currently recognized by
European civilization is badly made and must be revised from top to bottom. We
must proceed with what he calls the "transvaluation of all values" (Umwerthung
aller Werthe), consequently change the orientation of our whole life, change
the essential principles on which all our judgments are based. Towards the end
of his conscious life, his imagination, exalted by the deep solitude around him
and perhaps also by the approach of the crisis into which his reason was to
sink, saw in this philosophical revolution the starting point of a formidable
upheaval for humanity: "I swear to you," he wrote to Brandes on November 20,
1888, "that in two years' time the whole earth will be twisted into
convulsions. Ich bin ein Verhängniss. »

Modern man places at the top of his table of values a certain number of
absolute values, which he puts above all discussion and which serve as a
measure for appreciating all reality. Among these universally recognized goods
are, for example, the True and the Good. If there is one fact that seems to be
beyond dispute, it is that truth is better than error; to prove an assertion,
any theory, to be false is to take away all credit from it; the cult of truth,
of sincerity at all costs, is perhaps one of our strongest beliefs. In the same
way, the most reckless thinkers have stopped in fear before the problem of good
and evil. Kant looked upon the existence of his categorical imperative, "act in
such a way that your conduct may be established as a universal rule," as a
truth superior to all reason and discussion. Schopenhauer himself, while
criticizing Kant's theory of duty, nonetheless admitted that all men agree,
practically speaking, to formulate the content of the moral law in this way.
Neminem læde, immo omnes, quantum potes, juva: "Do no harm to anyone, help
others as much as you can. "Philosophers have never dared to question the
legitimacy of moral judgments, they have only been concerned with seeking the
"foundation of morality", with seeking the rational - practically completely
indifferent - reason for these judgments constantly and on all human actions in
the name of a "moral conscience" before which everyone bows down with respect.
It is precisely to these convictions, which today dominate the inner life of
almost all men, to this cult of truth, to this religion of moral law, that
Nietzsche declares war. Instead of respectfully accepting them as a fact that
is useless to discuss, as an authority whose titles it is unholy to examine, he
boldly considers them as a problem, he does not fear to ask himself clearly the
quesjion: Why truth rather than error? Why good rather than evil? And the
problem thus posed he solves with the same boldness by setting as a rule of
conduct for a truly free man the motto of that mysterious order of "Assassins"
that the Crusaders once encountered in the Holy Land: "Nothing is true;
everything is permitted. »

For Nietzsche in fact all these metaphysical, mysterious and superhuman
entities that man has always assumed outside himself and that he has revered
under various names - "God", the world of "Things in itself", the "Truth", the
"Categorical Imperative" - are only ghosts of our imagination. The most
immediate reality, the only reality we are given to know, is the world of our
desires, of our passions. All our actions, all our wills, all our thoughts are
in the last analysis governed by our instincts, and these instincts all come
down, in the end, to a single primordial instinct, the "will to power" which is
sufficient - this is Nietzsche's hypothesis - to explain alone all the
manifestations of life that we witness. Every living being - plant, animal or
human - tends to increase its strength by submitting to its domination other
beings, other forces. This continuous effort, this perpetual struggle in which
each being unceasingly puts his own life at stake to increase his power, is the
fundamental law of all existence. All manifestations of life without exception
are governed by instinct. If man aspires to virtue, truth or art it is by
virtue of a natural instinct which, in order to satisfy himself, pushes him to
act in a certain way. Thus the morality which the Christian regards as a divine
revelation and to which he subordinates his whole existence is in reality a
human invention intended to satisfy this or that instinct. In the same way the
truth to which the scientist devotes his life was sought in the first place by
the will to power which tended to increase his domination. But man has come, by
a singular aberration, to worship as an ideal what he has created himself to
meet one of his needs. Instead of saying: "I live to satisfy my instincts, and
by virtue of this law I will therefore seek the good and the true to the extent
that my will to power will drive me to it," he states in principle: "Good and
true must be sought for their own sake; one must do good because it is good,
aspire to truth for the love of truth; man's life has value only in so far as
he subordinates his selfish interest to this ideal goal; he must therefore, in
the name of the ideal, compress his personal instincts and regard selfishness
as evil. "Now, the man who reasons like this and acts accordingly, is in truth
also driven by an instinct - for instinct is the ultimate motive for all our
actions - only this instinct is perverted.

Not all human instincts are, in fact, equally healthy; some are normal and tend
to increase his vitality, but others are morbid and tend to weaken it. Diseases
of the body have natural causes and develop according to the laws of the
organism; they nevertheless lead to the destruction of the body and must
therefore be fought against by the physician. The same is true of personality
diseases: they have a natural origin, but their consequences are no less
disastrous. Depending on whether normal or morbid instincts will predominate in
a given individual, he will be a fine example of humanity or a degenerate.
There are, therefore, on the one hand, men who are healthy in body and soul,
who say "yes" to existence, who are happy to live and worthy of perpetuating
life, and there are, on the other hand, the sick, the helpless, the decadent,
whose vital instincts are diminished, who say "no" to existence, who incline
towards death, towards annihilation, who no longer seek, or in any case should
no longer seek, to perpetuate themselves. This is a natural and physiological
reality against which there is no need to rebel: in fact, life is everywhere in
progress or decadence, it increases or decreases in intensity; man is a plant
that sometimes vegetates miserably and sometimes flourishes splendidly, growing
powerful and magnificent offspring on all sides. - It is on this fact that
Nietzsche bases his table of values.

He reasoned as follows: "I don't know whether life itself is good or bad.
Nothing is more vain, in fact, than the eternal discussion between optimists
and pessimists, and for an excellent reason: no one in the world is qualified
to judge what life is worth: the living cannot because they are part of the
debate and even objects of dispute; nor can the dead - because they are
dead. What life is worth in its totality, therefore, no one can say; I
will forever ignore whether it would have been better for me to be or not to
be. But from the moment I live, I want life to be as exuberant, as luxuriant,
as tropical as possible, inside and outside of me. So I will say "yes" to
everything that makes life more beautiful, more worth living, more intense. If
it is shown to me that error and illusion can serve the development of life, I
will say "yes" to error and illusion; If it is shown to me that the instincts
described as "bad" by present-day morality - for example, harshness, cruelty,
cunning, daring, reckless daring, fighting spirit - are such as to increase
man's vitality, I will say "yes" to evil and sin; if it is shown to me that
suffering contributes as much as pleasure to the education of mankind, I will
say "yes" to suffering. - On the contrary, I will say "no" to everything that
diminishes the vitality of the human plant. And if I discover that truth,
virtue, goodness, in a word all the values revered and respected by men until
now are harmful to life, I will say "no" to science and morality. »

## Colle 2

Get into the habit of thinking that death means nothing to us. For all good and
all evil resides in sensation: and death is the deprivation of all sensibility.
Therefore, the knowledge of this truth that death is nothing for us, makes us
capable of enjoying this mortal life, not by adding to it the perspective of
infinite duration, but by removing from us the desire for immortality. For
there is nothing left to fear in life, for those who have truly understood that
outside of life there is nothing to fear. It is therefore vain to say that
death is to be feared, not because it will be painful being realized, but
because it is painful to wait for it. It would indeed be a vain and pointless
fear to fear the one that would be produced by waiting for something that
causes no trouble by its presence.

Thus that of all evils which gives us the most horror, death, is nothing to us,
since, as long as we exist ourselves, death is not, and when death exists, we
are no longer. So death exists neither for the living nor for the dead, since
it has nothing to do with the former, and the latter are no more.

But the multitude sometimes flees death as the worst of evils, sometimes calls
it the end of the evils of life. The wise man, on the contrary, does not
disregard life, nor is he afraid of no longer living: for life is not his
responsibility, nor does he consider it the least evil to no longer live. Just
as it is not always the longest time that one wants to collect, but the most
pleasant. As for those who advise young people to live well and old people to
die well, their advice is meaningless, not only because life is good even for
the old man, but because the care to live well and the care to die well are one
and the same. It is even worse when it is said that it is good not to be born,
or, once born, to pass through the gates of Hades as quickly as possible. For
if the man who uses this language is convinced, how can he not come out of
life? This is indeed something that is always within his reach, if he wants his
death with a firm will. That if this man jokes, he shows lightness in a subject
that has none.

Remember that the future is neither ours nor yet completely out of our grasp,
so that we must neither count on it as if it were bound to happen, nor forbid
ourselves any hope, as if it were sure it should not be.

## Colle 3

> On the disaster, Gaëlle Clavandier. A look back at the disaster.
The Gantry [Online], 22 | 2009.

The catastrophe is only worthwhile because of its temporal nature, hence its
event-driven character. However, its memorial construction and especially the
commemorations mean that different registers of temporality are summoned. The
metaphor of mourning and resilience is operative in many situations. Referring
only to urgency means losing sight of the fact that the rupture coagulates with
more regular registers of temporality, and also means forgetting that the
apprehension of ruptures is organized in series. Thus, in addition to the
emergency scenario, phenomenological and cyclical temporalities, experienced
and not the result of history, are variables to be experimented with.

The catastrophe only makes sense if it is referred to a territory (the place of
its production), inasmuch as its material consequences, its dangerousness, its
aura often transcends administrative or even identity-based boundaries. The
local dimension, the one that the anthropologist usually works on, must be
revisited by other scales. Thus, for the tragedy of the "5/7" (a fire that
claimed nearly 150 victims in 1970), the disaster was divided into several
territories, which may or may not be spatialized: parents of the victims,
residents of the "indirectly" affected commune, young people from Grenoble and
Chambéry. This dimension operates a link with the next, because the productions
of discourse are localized.

A break in intelligibility, the catastrophe is also a possible return of
meaning. It only exists when it is part of a process of signification. In this
respect, thinking of disaster narratives as external to the dramatic event is a
misunderstanding, because the disaster carries within it (etymologically
speaking) its outcome or, in other words, its assumption of responsibility. The
narration, in words and images, the presence of certain silences, leads to
saying, quantifying, and taking the measure of the facts. For all that, this
narrative is not univocal and can evolve little.

Ultimate dimension, death, the disappeared and disfigured bodies, the disorder
produced by the disaster which carries "primary" emotions (grasping the
upheaval that any disaster constitutes) and "secondary" affects (feelings,
elaborated perceptions in the face of the drama) lead us to rethink the
analytical dimensions that are time, space and narratives. From the point of
view of the survey, the researcher is a stakeholder in this regime since he
meets the affected audiences. This sensitive dimension must be reflected at the
scale of observation, but also in the description and interpretation of the
data. This is a far cry from the expert posture that wants to cut itself off
from "emotion" in order to approach reason.

## Colle 4

> André GIDE, Earthly Foods, II (1897)

The man who says he is happy and thinks, that one will be called really strong.

Nathaniel, the misfortune of each one comes from the fact that it is always
each one who looks and subordinates to him what he sees. It is not for us, it
is for her that everything is important. Let your eye be the thing looked at.

Nathaniel! I can no longer begin a single line without your delicious name
coming back to it. Nathaniel, I would like to bring you to life.

Nathaniel, do you understand enough the pathetic nature of my words? I would
like to come closer to you even more.

[…]

Nathaniel, I want to teach you fervor.

Nathanael, for do not abide with that which is like you; never abide,
Nathanael. As soon as someone else has taken your likeness, or you have made
yourself like someone else, it is no longer profitable for you. You must leave
them. Nothing is more dangerous for you than your family, your room, your past.
Take from each thing only the education it brings you, and let the pleasure
that flows from it dry up.

Nathaniel, I will tell you about the moments. Have you understood how strong
their presence is? A not constant enough thought of death has not given enough
price to the smallest moment of your life. And don't you understand that every
moment would not take on that admirable, if not detached brilliance, so to
speak, against the very dark background of death?

I would no longer seek to do anything, if I were told, if it were proven to me,
that I have all the time in the world to do so. I would rest first of all from
having wanted to start something, having time to do all the others as well.
What I would do would never be just anything, if I didn't know that this form
of life must end - and that I would rest from it, having lived it, in a sleep a
little deeper, a little more forgetful than the one I expect from every
night...

And so I got into the habit of separating every moment of my life, for a
totality of joy, isolated; to suddenly concentrate a whole particularity of
happiness in it; so that I no longer recognized myself from the most recent
memory.

## Colle 5

> André MALRAUX, Anti-memories (1972)

Reflecting on life - on life in the face of death - is probably nothing more
than deepening one's questioning. I'm not talking about being killed, which
hardly raises any questions for anyone who has the banal good fortune to be
courageous, but about the death that arises in everything that is stronger than
man, in the aging and even the metamorphosis of the earth (the earth suggests
death by its millennial torpor as well as by its metamorphosis, even if its
metamorphosis is the work of man) and above all the irremediable, the: you will
never know what all this meant. In the face of this question, what does it
matter to me? Almost all the writers I know love their childhood, I hate mine.
I have learned little and badly to create myself, if to create myself is to
make do with that roadless inn called life. I have sometimes been able to act,
but the interest of action, except when it rises to the level of history, is in
what we do and not in what we say. I'm not very interested. Friendship, which
has played a great role in my life, has not accommodated curiosity. […]

Why remember me?

Because, having lived in the uncertain realm of the spirit and fiction that is
that of the artist, then in that of combat and history, having experienced at
the age of twenty an Asia whose agony still brought to light what the West
meant, I have encountered many times, sometimes humble and sometimes dazzling,
those moments when the fundamental enigma of life appears to each of us as it
appears to almost all women before the face of a child, to almost all men
before the face of death. In all the forms of what drags us along, in all that
I have seen struggle against humiliation, and even in you, a gentleness about
which one wonders what you are doing on earth, life like the gods of the
religions that have disappeared sometimes appears to me as the libretto of an
unknown music. […]

The feeling of becoming a stranger to the earth, or of returning to the earth,
which we find here on several occasions, seems to be born, most often, from a
dialogue with death. Being the object of a mock execution does not bring a
negligible experience. But I owe this feeling first of all to the singular,
sometimes physical, action that the bewitching consciousness of the centuries
exerts on me. Awareness made more insidious by my work on art, because any
Imaginary Museum brings both the death of civilizations and the resurrection of
their works. I always believe I write for men who will read me later. Not by
confidence in this book, not by obsession with death or History as the
intelligible destiny of humanity: by the violent feeling of an arbitrary and
irreplaceable drift like that of the clouds. Why do I note my interviews with
heads of state rather than with others? Because no conversation with a Hindu
friend, even one of the last sages of Hinduism, makes me as sensitive as Nehru
does when he says to me: "Gandhi thought that...". If I mix these men, the
temples and the tombs, it is because they express in the same way "what
passes". When I listened to General de Gaulle, during the most banal lunch in
his private apartment at the Elysée Palace, I thought: today, around 1960... At
official receptions, I thought of those in Versailles, the Kremlin, Vienna at
the end of the Habsburgs. In Lenin's modest office, where dictionaries form the
base of the small bronze pithecanthrope offered by an American Darwinist, I was
not thinking of prehistory, but of the mornings when that door was pushed open
by Lenin - the day when in the courtyard downstairs he had started dancing on
the snow, shouting to Trotsky in amazement: "Today we have lasted one day
longer than the Paris Commune! "Today... In front of the start of France as in
front of the poor pithecanthrope, I was fascinated by the centuries, by the
trembling and changing glow of the sun on the course of the river... In front
of the sign of the glove-maker of Bône when I was returning from my first walk
towards death, as in Gramat when I was carried on a stretcher to pretend to
shoot me, as in front of my cat's sneaky slide, how many times have I thought
what I thought of India : in 1938, or in 1944, or in 1968, before Christ...

## Colle 6

> Jean-Jacques ROUSSEAU, Les Rêveries du Promeneur solitaire,
Cinquième Promenade (1778)

Everything is in a continuous flow on earth: nothing keeps a constant and
arrested form, and our affections that are attached to external things pass and
change necessarily as they do.

Always ahead or behind us, they remind us of the past which is no longer or
warn us of the future which often should not be: there is nothing solid to
which the heart can attach itself. There is nothing solid to which the heart
can be attached. Therefore, here on earth we have little more than passing
pleasure; for the happiness that lasts I doubt it is known. Hardly is there a
moment in our most vivid pleasures when the heart can truly say to us: I would
like this moment to last forever; and how can one call happiness a fleeting
state that still leaves us with an anxious and empty heart, that makes us
regret something before, or still desire something after? But what if there is
a state where the soul finds a plate solid enough to rest on and gather its
whole being there, without needing to recall the past nor to step over the
future; where time is nothing for it, where the present always lasts without
nevertheless marking its duration and without any trace of succession, without
any other feeling of deprivation or enjoyment, pleasure or sorrow, desire or
fear than that alone of our existence, and that this feeling alone can fill it
up completely ; as long as this state lasts he who is in it may be called
happy, not of an imperfect, poor and relative happiness such as is found in the
pleasures of life, but of a sufficient, perfect and full happiness, which
leaves no emptiness in the soul which it feels the need to fill. Such is the
state in which I often found myself on the island of Saint-Pierre in my
solitary daydreams, either lying in my boat that I let drift at the mercy of
the water, or sitting on the shores of the turbulent lake, or elsewhere on the
banks of a beautiful river or stream whispering on the gravel.

What do you enjoy in such a situation? Nothing outside of oneself, nothing but
oneself and one's own existence, as long as this state lasts one is
self-sufficient like God. The feeling of existence stripped of all other
affection is in itself a precious feeling of contentment and peace, which alone
would be enough to make this existence dear and sweet to those who know how to
remove from themselves all the sensual and earthly impressions that constantly
come to distract us from it and disturb its sweetness here below. But most men,
agitated by continual passions, know little about this state, and having tasted
it only imperfectly for a few moments only retain an obscure and confused idea
of it which does not make them feel its charm. It would not even be good, in
the present constitution of things, for them to be eager for these sweet
ecstasies and to be disgusted with the active life whose ever-renewing needs
prescribe their duty.
