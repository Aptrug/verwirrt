# Hommes préparatoires

## aus <https://www.reddit.com/r/Nietzsche/comments/g1qarv/the_gay_science_humanity_and_history_616/>

Nietzsche se réjouit des signes annonciateurs d'une ère plus guerrière
(il voit la montée du nationalisme en Europe qui conduira à la
Grande Guerre et, à son tour, à la Seconde Guerre mondiale). Nietzsche
n'était pas nationaliste lui-même, mais il croit que les conflits
imminents ouvriront la voie à une ère plus élevée qui fera des
guerres au nom de la connaissance et des idées (voir à nouveau
le point 4). À cette fin, nous avons besoin d'êtres humains préparatoires — des
personnes dont le rôle est de préparer une histoire plus
élevée, des personnes qui s'efforceront de se dépasser et de surmonter
les choses en général. La façon de tirer le meilleur parti de l'existence,
la façon de tirer le meilleur parti de la vie, est de vivre dangereusement.
Vivre dangereusement causera des dommages et la mort de nombreux
individus, mais c'est le prix à payer pour le progrès, qui profitera
à l'humanité. Nietzsche ne veut pas que l'humanité soit timide
ou timide - nous devons explorer et nous remettre en question.
Un exemple en dehors de la guerre est l'exploration, de ceux qui
ont entrepris des voyages à travers la mer au programme spatial
moderne - beaucoup d'individus ont perdu la vie, et beaucoup d'autres
le feront, dans notre quête de connaissances et de nouvelles expériences.

L'idée d'êtres humains préparatoires peut soutenir une certaine
interprétation de l'Ubermensch en tant qu'objectif pour l'humanité
; pour dire les choses crûment : étant donné que les traits de
caractère sont héréditaires, les gens devraient se considérer comme
des êtres préparatoires à surmonter et se reproduire eux-mêmes
afin d'amener une humanité supérieure et finalement l'Ubermenschen.
Personnellement, je pense que le concept de l'Ubermensch de Nietzsche
est la création consciente par Nietzsche de sa propre figure idéale
(mais irréelle), la façon dont les Grecs ont créé leurs dieux et
leurs héros, les Juifs ont créé leurs figures idéales, etc. En
attendant, pour en savoir plus sur l'Ubermensch, consultez ce grand
article de /u/SheepwithShovels qui fait partie des ressources de
ce sous-reddit.

## aus arasite.org/nietgaysci.html

Une "époque plus virile, plus guerrière" (228) est à saluer parce qu'elle
redonnera honneur et courage.  Une nouvelle ère s'annonce où la recherche de la
connaissance semblera héroïque et la guerre comme on la combat pour le bien des
idées.  Nous devons encourager le développement d'êtres humains suffisamment
courageux, malgré les effets néfastes de la civilisation moderne.  Ils seront
joyeux, patients, sans prétention et non vaniteux.  Ils marcheront à leur propre
rythme et seront prêts à commander et à obéir.  Ils doivent vivre dangereusement
et prendre des risques dans leur quête de connaissances, et ils arriveront au
pouvoir.
