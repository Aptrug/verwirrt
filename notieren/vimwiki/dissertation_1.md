# Dissertation 1

## Sujet

notre vie n'est pas seulement une vie au sens biologique,
mais elle est une existence.[^1]

[^1]: Pierre-Henry Frangne, Penser la mort et le deuil

## initiation à la dissertation

### Introduction

Comme c'est le cas pour la plupart des sujets philosophiques,
aborder la question de vie d'un seul côté est découragé.
C'est dans le même ordre d'idées que Pierre-Henry Frangne a dit
"notre vie n'est pas seulement une vie au sens biologique,
mais elle est une existence". En employant une négation
et une concession, l'auteur semble rejeter la perspective biologique
où le terme de la vie semble être inséparable avec les cellules,
les organes et les tissus.
La vie pour l'auteur est plus approfondie, c'est une existence.
Ce qui nous amène à interroger: la vie est-elle un concept plutôt empirique
ou bien philosophique. Afin de trouver des réponses possibles à un telle
interrogation et en basant sur les trois œuvres au programme, à savoir
*les Contemplations* de Victor Hugo, *le Gai Savoir* de Friedrich Nietzsche
et *la Supplication* de Svetlana Aleksievitch, nous allons montrer que
certes la vie est une existence, mais elle aussi une notion plus inclusive
qui englobe davantage les différentes sciences, qu'elles soient
humaines, sociales ou naturelles.

### Conclusion

Bref, on peut dire que la vie est bien plus grande que d'être
réduite à une seule propriété ou à un seul domaine d'étude,
c'est un terme si familier et pourtant si mystérieux dont
le sens reste loin de la portée de la science car il n'admet
pas d'objectivité, et donc varie selon les expériences et
les croyances de l'individu, c'est qu'illustre Nietzsche en disant dans
la volonté de puissance: "il n'y a pas de faits, seulement des interprétations".
