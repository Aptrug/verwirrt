# Exercice 3

## 1

### fct(32)

* 16
* 8
* 4
* 2
* 1

### fct(20)

* 10
* 5
* 2
* 1

## 2

### meine Antwort

$$
f(n) = \left \lfloor{log_2(n)}\right \rfloor
$$

### ihre Antwort

soit $f(n)$ le nombre d'itérations élémentaires et T le nombre d'itérations \
$f(n) = T + 1 + 2T + 1 = 3T + 2$ \
soit le tableau suivant
itération | données
|---------|--------
1         | $n/2$
2         | $n/2^2$
.         | .
.         | .
.         | .
T         | $n/2^T$
$\dfrac{n}{2^T} = 1 \implies n = 2^T$ \
d'où $T = log_2(n)$ \
d'où la complexité est $O(log_2(n))$
