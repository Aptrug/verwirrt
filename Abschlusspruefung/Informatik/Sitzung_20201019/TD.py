# 1

# solution recursive
def nombreNoeuds1(arb):
    if arb == []:
        return 0
    else:
        return 1 + nombreNoeuds1(arb[1]) + nombreNoeuds1(arb[2])


# solution iterative
def nombreNoeuds2(arb):
    pile = [arb]
    nb = 0
    while pile != []:
        N = pile.pop()
        nb += 1
        if N[1] != []:
            pile.append(N[1])
        elif N[2] != []:
            pile.append(N[2])
        return nb


# 2

def nbreFeuilles(arb):
    if arb == []:
        return 0
    elif arb[1] == [] and arb[2] == []:
        return 1
    else:
        return nbreFeuilles(arb[1]) + nbreFeuilles(arb[2])


# 3

def sommeVal(arb):
    if arb == []:
        return 0
    else:
        return arb[0] + sommeVal(arb[1]) + sommeVal(arb[2])


# testing

Arb = [15, [7, [6, [], []], [9, [], []]], [20, [], [25, [], []]]]
print('Le nombre des noeuds: ', nombreNoeuds1(Arb))
print('Le nombre des noeuds feuilles: ', nbreFeuilles(Arb))
print('La somme des noeuds: ', sommeVal(Arb))
