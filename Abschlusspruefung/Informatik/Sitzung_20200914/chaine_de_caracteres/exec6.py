chiffres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


def convert2Nbre(ch):
    nombre = 0
    for i in range(len(ch)):
        for j in chiffres:
            if ch[::-1][i] == str(j):
                nombre += j*10**i
    return nombre


print(convert2Nbre('54032'))
