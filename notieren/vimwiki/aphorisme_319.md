# En interprètes de nos expériences vécues

## aus arasite.org/nietgaysci.html

Nous devons être honnêtes et réfléchis sur nos expériences, en célébrant le fait
de pouvoir raisonner à leur sujet au lieu d'aller à l'encontre de la raison
comme le font les religieux.  Nous devrions traiter l'expérience comme une sorte
d'expérience scientifique.
