# Développements limités usuels en 0

## À l'ordre 1

$$ e^x = 1 + x + o(x) $$
$$ (1 + x)^\alpha = 1 + \alpha x + o(x) $$
$$ \dfrac{1}{1 + x} = 1 - x + o(x) $$
$$ \ln(1 + x) = x + o(x) $$
$$ \sqrt{1 + x} = 1 + \dfrac{x}{2} + o(x) $$
$$ \dfrac{1}{\sqrt{1 + x}} = 1 - \dfrac{x}{2} + o(x) $$
$$ \sin(x) = x + o(x) $$
$$ \cos(x) = 1 + o(x) $$
$$ \tan(x) =  x + o(x) $$
$$ \arcsin(x) = x + o(x) $$
$$ \arccos(x) = \dfrac{\pi}{2} - x + o(x) $$
$$ \arctan(x) = x + o(x) $$
$$ \sinh(x) = x + o(x) $$
$$ \cosh(x) = 1 + o(x) $$
$$ \tanh(x) = x + o(x) $$
$$ \operatorname{arsinh}(x) = x + o(x) $$
$$ \operatorname{artanh}(x) = x + o(x) $$

\pagebreak

## À l'ordre 2

$$ e^x = 1 + x + \dfrac{x^2}{2} + o(x^2) $$
$$ (1 + x)^\alpha = 1 + \alpha x + \dfrac{\alpha(\alpha - 1)}{2} x^2 + o(x^2) $$
$$ \dfrac{1}{1 + x} = 1 - x + x^2 + o(x^2) $$
$$ \ln(1 + x) = x - \dfrac{x^2}{2} + o(x^2) $$
$$ \sqrt{1 + x} = 1 + \dfrac{x}{2} - \dfrac{x^2}{8} + o(x^2) $$
$$ \dfrac{1}{\sqrt{1 + x}} = 1 - \dfrac{x}{2} + \dfrac{3}{8} x^2 + o(x^2) $$
$$ \sin(x) = x - \dfrac{x^3}{6} + o(x^3) $$
$$ \cos(x) = 1 - \dfrac{x^2}{2} + o(x^2) $$
$$ \tan(x) =  x + \dfrac{x^3}{3} + o(x^3) $$
$$ \arcsin(x) = x + \dfrac{x^3}{6} + o(x^3) $$
$$ \arccos(x) = \dfrac{\pi}{2} - \arcsin(x) $$
$$ \arctan(x) = x - \dfrac{x^3}{3} + o(x^3) $$
$$ \sinh(x) = x + \dfrac{x^3}{6} + o(x^3) $$
$$ \cosh(x) = 1 + \dfrac{x^2}{2} + o(x^2) $$
$$ \tanh(x) = x - \dfrac{x^3}{3} + o(x^3) $$
$$ \operatorname{arsinh}(x) = x - \dfrac{x^3}{6} + o(x^3) $$
$$ \operatorname{artanh}(x) = x + \dfrac{x^3}{3} + o(x^3) $$
