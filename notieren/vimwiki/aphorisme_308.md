# L’histoire de tous les jours

## aus arasite.org/nietgaysci.html

Si nous examinons notre vie quotidienne, nous pouvons constater qu'elle est
toujours un mélange de courage et de paresse.  Les éloges ou la respectabilité
peuvent nous aider à développer une bonne conscience, mais cela ne sera pas le
cas pour les hommes de science.
