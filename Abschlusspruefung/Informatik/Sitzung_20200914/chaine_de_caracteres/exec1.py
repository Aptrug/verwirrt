# Inverser une chaîne de caractère en

def inverserChaine(ch0):
    ch1 = ''
    for a in ch0:
        ch1 = a + ch1
    return ch1


question = float(input('Question: '))
if question == 1.:
    ch0 = 'Bonjour'
    print(inverserChaine(ch0))
elif question == 2.:
    chaine = str(input('Donner la chaine: '))
    print('La chaine devient: ', inverserChaine(chaine))
