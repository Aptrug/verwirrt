# rotation d'une liste (elle s'appelle en fait "translation")

# 1


def rotationGauche(ls, nb):
    nb = nb % len(ls)
    return ls[nb:] + ls[:nb]


# 2

ls = [45, 25, 6, 78, 15, 87, 65, 37, 14]
nb = 3
ls = rotationGauche(ls, nb)
print("la liste devient: ", ls)
