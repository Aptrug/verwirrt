# helper function


# 1
# solution recursive
def nombreNoeuds1(arb):
    if arb == []:
        return 0
    else:
        return 1 + nombreNoeuds1(arb[1]) + nombreNoeuds1(arb[2])


# solution iterative
def nombreNoeuds2(arb):
    pile = [arb]
    nb = 0
    while pile != []:
        N = pile.pop()
        nb += 1
        if N[1] != []:
            pile.append(N[1])
        elif N[2] != []:
            pile.append(N[2])
        return nb


# 2
def nbreFeuilles1(arb):
    if arb == []:
        return 0
    elif arb[1] == [] and arb[2] == []:
        return 1
    else:
        return nbreFeuilles1(arb[1]) + nbreFeuilles1(arb[2])


# solution iterative
def nbreFeuilles2(arb):
    return len(arb)


# 3
def sommeVal1(arb):
    if arb == []:
        return 0
    else:
        return arb[0] + sommeVal1(arb[1]) + sommeVal1(arb[2])


# solution iterative
def sommeVal2(L):
    total = 0
    for i in L:
        if isinstance(i, list):
            total += sommeVal2(i)
        else:
            total += i
    return total


# 4
def nbreValPair1(arb):
    if arb == []:
        return 0
    elif arb[0] % 2 == 0:
        nb = 1
    else:
        nb = 0
    return nb + nbreValPair1(arb[1]) + nbreValPair1(arb[2])


# solution iterative
def nbreValPair2(L):
    total = 0
    for i in L:
        if isinstance(i, list):
            total += nbreValPair2(i)
        elif i % 2 == 0:
            total += 1
    return total


# 5
def hauteur1(arb):
    if arb == []:
        return 0
    x = hauteur1(arb[1])
    y = hauteur1(arb[2])
    return 1 + max(x, y)


# solution iterative
def hauteur2(lst):
    if type(lst) is not list:
        return 0
    max = len(lst)
    for i in lst:
        max_i = hauteur2(i)
        if max_i > max:
            max = max_i
    return max


# 6
def minValeur1(arb):
    if arb[1] != [] and arb[2] == []:
        return min(arb[0], minValeur1(arb[1]))
    if arb[1] == [] and arb[2] != []:
        return min(arb[0], minValeur1(arb[2]))
    if arb[1] == [] and arb[2] == []:
        return arb[0]
    if arb[1] != [] and arb[2] != []:
        x = minValeur1(arb[1])
        y = minValeur1(arb[2])
    return min(arb[0], min(x, y))


# solution iterative
def minValeur2(arb):
    return min(afficheArbre2(arb))


# 7
def afficheArbre1(arb):
    if arb != []:
        afficheArbre1(arb[1])
        print(arb[0])
        afficheArbre1(arb[1])


# solution iterative
def afficheArbre2(arb):
    total = []
    k = None
    for i in arb:
        if isinstance(i, list):
            total += afficheArbre2(i)
        else:
            k = 0
            k += i
    if k is not None:
        total += [k]
    return total


# 8
def insererValeur(arb, v):
    if arb == []:
        arb.append(v)
        arb.append([])
        arb.append([])
    elif arb[0] > v:
        insererValeur(arb[1], v)
    else:
        insererValeur(arb[2], v)


# solution iterative

# 9
def trouverNoeud1(arb, x):
    if arb == []:
        return False
    if arb[0] == x:
        return True
    if trouverNoeud1(arb[1], x) is True or trouverNoeud1(arb[2], x) is True:
        return True
    return False


# solution iterative
def trouverNoeud2(arb, x):
    if x in afficheArbre2(arb):
        return True
    else:
        return False


# 10
def maxValeur1(arb):
    if arb[2] == []:
        return arb[0]
    return maxValeur1(arb[2])


# solution iterative
def maxValeur2(arb):
    return max(afficheArbre2(arb))


# 11
def tab2arbre(tab):
    arb = []
    if tab == []:
        return arb
    else:
        for x in tab:
            insererValeur(arb, x)
        return arb


# testing

tab = [6, 2, 3, 20, 22, 14, 28]
Arb = [15, [7, [6, [], []], [9, [], []]], [20, [], [25, [], []]]]
# print("Le nombre des noeuds: ", nombreNoeuds1(Arb))
# print("Le nombre des noeuds: ", nombreNoeuds2(Arb))
# print("Le nombre des noeuds feuilles: ", nbreFeuilles1(Arb))
# print("Le nombre des noeuds feuilles: ", nbreFeuilles2(Arb))
# print("La somme des noeuds: ", sommeVal1(Arb))
# print("La somme des noeuds: ", sommeVal2(Arb))
# print(nbreValPair1(Arb))
# print(nbreValPair2(Arb))
# print(hauteur1(Arb))
print(hauteur2(Arb))
# print(minValeur1(Arb))
# print(minValeur2(Arb))
# print(afficheArbre1(Arb))
# print(afficheArbre2(Arb))
# print(insererValeur(Arb, 3))
# print(insererValeur(Arb, 97))
# print(trouverNoeud1(Arb, 47))
# print(trouverNoeud2(Arb, 47))
# print(maxValeur1(Arb))
# print(maxValeur2(Arb))
# print(tab2arbre(tab))
