# Le mauvais quart d’heure

## aus arasite.org/nietgaysci.html

Les formes normales d'interaction ne nous donnent qu'une vision unilatérale des
événements.  La contemplation peut nous aider à voir la sagesse partagée dans
ces vues, tout en préservant le rôle de l'instinct.  C'est de toute façon ainsi
que se développe l'intelligence humaine, de sorte que l'instinct joue un rôle
important.  La pensée consciente est désormais valorisée, mais nous savons
qu'une grande partie de l'activité est "inconsciente et non ressentie"(262).
Bien sûr, la contestation des instincts peut provoquer l'épuisement du penseur,
et il faut peut-être être héroïque pour y parvenir, mais cela n'a rien à voir
avec l'essence de l'humanité, plus que la pensée consciente est la forme la plus
douce et la moins nocive.
