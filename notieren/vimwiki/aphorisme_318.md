# Sagesse dans la douleur

## aus arasite.org/nietgaysci.html

La douleur peut apporter la sagesse et c'est pourquoi la douleur est essentielle
à l'espèce humaine, comme un avertissement.  Certains types d'héroïsme
apprécient cependant la douleur comme un signe de leur mépris pour le confort et
le bonheur normaux.
