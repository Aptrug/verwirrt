import numpy as np


question = float(input('Question: '))
if question == 1.:
    def matrice1(n):
        A = np.zeros((n, n))
        for i in range(n):
            A[i, i] = 2
        for i in range(n - 1):
            A[i, i + 1] = 1
            A[i + 1, i] = 1
        return A
    A = matrice1(5)

if question == 2.:

    def matrice2(n):
        L = []
        for i in range(n):
            L1 = []
            for j in range(n):
                if i == j:
                    L1.append(2)
                elif j == i + 1 or i == j + 1:
                    L1.append(1)
                else:
                    L1.append(0)
            L.append(L1)
        return np.array(L)
    A = matrice2(5)


if question == 3.:
    def matrice3(n):
        A1 = np.diag(np.ones(n) * 2)
        A2 = np.diag(np.ones(n - 1), 1)
        A3 = np.diag(np.ones(n - 1), -1)
        return A1 + A2 + A3
    A = matrice3(5)


print(A)
