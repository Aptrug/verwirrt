# Architecture des hommes de connaissance

## aus arasite.org/nietgaysci.html

Nous avons besoin de bien meilleurs bâtiments et d'un meilleur aménagement de
nos villes, qui encourageront la réflexion et la contemplation.  Les bâtiments
d'église ne sont pas bons parce qu'ils sont contaminés par le christianisme qui
domine la pensée.
