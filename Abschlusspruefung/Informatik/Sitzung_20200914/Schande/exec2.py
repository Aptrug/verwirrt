import numpy as np

import pylab as plb

import tikzplotlib

t = np.linspace(0, 2 * np.pi, 200)
x = np.sin(t) / (1 + np.cos(t) ** 2)
y = np.sin(t) * np.cos(t) / (1 + np.cos(t) ** 2)
plb.plot(x, y)
tikzplotlib.save('exec2.tex')
