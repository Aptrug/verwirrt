# Contre les calomniateurs de la nature

## aus <https://www.reddit.com/r/Nietzsche/comments/fzw6iv/the_gay_science_noble_and_common_516/>

Pour certaines personnes - celles qui sont communes - tout penchant
naturel est considéré comme une maladie ; leurs instincts, leurs
passions, leurs désirs et leurs émotions les dépassent ou les corrompent.
L'histoire de la philosophie morale s'accorde presque entièrement
sur le fait que nos inclinaisons naturelles doivent être contrôlées
par notre moi rationnel ; des exemples notables sont Platon, Aristote,
les Stoïciens, Saint Paul et Kant. Ce sont ces personnes qui ont
répandu l'idée que nos penchants naturels sont mauvais. C'est une
marque de noblesse que de ne pas craindre sa propre nature et de
ne rien attendre d'infâme de soi-même - les gens nobles volent
sans scrupule là où nous avons envie de voler, nous, les oiseaux
nés libres (les esprits libres ?). Compte tenu de leur conception
de la moralité, les noms ci-dessus partagent des points de vue
avec le type commun, mais certains d'entre eux recherchent également
des connaissances abstraites et la beauté pour eux-mêmes, qu'ils
partageraient avec le type supérieur, ce qui semble impliquer qu'être
Noble ou Commun n'est pas un absolu ou l'un ou l'autre, ce qui
a du sens si c'est une question de nature (plus sur l'aspect héréditaire
du caractère la prochaine fois dans la partie 6).

## aus arasite.org/nietgaysci.html

Beaucoup de gens pensent que tout ce qui est naturel est contre l'homme et le
mal.  Seuls les hommes nobles peuvent y échapper, car cela ne nécessite aucune
peur de soi et d'une éventuelle infamie, une capacité à voler sans contrainte.
