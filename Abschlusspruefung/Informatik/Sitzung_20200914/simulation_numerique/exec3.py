import numpy as np

A = np.array([[2, -1, -3], [3, 2, -2], [-1, -4, 6]])
B = np.array([1, 8, -32])
X = np.linalg.solve(A, B)
print(X)
