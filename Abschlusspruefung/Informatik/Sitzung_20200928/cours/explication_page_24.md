soit $f(n)$  le nombre d'opérations élémentaires \
soit T le nombre d'itérations
$$
f(n) = 4 + (T + 1) + 6T + 1 = 7T + 6
$$
trouvant le T \
soit le tableau suivant
itération | données
|---------|--------
1         | $N/2$
2         | $N/2^2$
.         | .
.         | .
.         | .
T         | $N/2^T$
$$
N/2^T = 1 \\
2^T = N \\
T = \log_2(N) \\
\implies f(n) = 7 \log_2 (N) + 1 = O(log_2(N))
$$
