# matrix oder so

# 1
def SommeValMat(A):
    s = 0
    for i in range(len(A)):
        for j in range(len(A[0])):
            s += A[i][j]
    return s


# 2
M = []
L = int(input('Donner le nombre des lignes: '))
C = int(input('Donner le nombre des colonnes: '))
for i in range(L):
    ligne = []
    print('Ligne: ', i + 1)
    for _j in range(C):
        x = float(input('Donner une valeur: '))
        ligne.append(x)
    M.append(ligne)
s = SommeValMat(M)
print('la somme des valeurs: ', s)
