miniscule = 'abcdefghijklmnopqrstuvwxyz'
majuscule = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def indexe(ch, x):
    for i in range(len(ch)):
        if ch[i] == x:
            return i


def changerCasse(ch1):
    ch2 = ''
    for i in ch1:
        if i in miniscule:
            i = majuscule[indexe(miniscule, i)]
        elif i in majuscule:
            i = miniscule[indexe(majuscule, i)]
        ch2 += i
    return ch2


chaine = str(input('Donner la chaine CH: '))
print('La chaine CH devient: ', changerCasse(chaine))
