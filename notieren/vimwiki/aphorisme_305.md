# Maîtrise de soi

## aus <https://www.reddit.com/r/Nietzsche/comments/freubj/the_gay_science_critique_of_moralists_116/>

Les moralistes qui conseillent une maîtrise de soi austère font
que les gens deviennent comme des châteaux en ce sens qu'ils se
protègent de certains aspects d'eux-mêmes - ce point de vue suppose
que le noyau du Soi (par exemple, l'âme) doit être protégé des
autres aspects de soi, comme les désirs et les passions. (Par exemple,
Aristote et Kant affirment que l'essence du Soi implique la capacité
de raisonner, et qu'il ne faut pas laisser les désirs et les passions
corrompre le vrai Soi). Nietzsche suggère que l'on doit parfois
perdre le contrôle pour apprendre des autres et même de soi-même.

## aus arasite.org/nietgaysci.html

Souligner le besoin de maîtrise de soi ne fait qu'entraîner une irritabilité et
une méfiance constantes.  Si l'on atteint la grandeur, c'est au prix de
l'appauvrissement et des bonnes relations avec les autres.
