# Aux prédicateurs de morale

## aus arasite.org/nietgaysci.html

Les moralisateurs populistes, toujours en train de se battre pour la vertu et
l'âme, finiront par dévaloriser ces idées - "l'alchimie à l'envers"(235).  Si
nous disions que la morale est interdite, nous pourrions attirer davantage
d'âmes héroïques : la peur aura remplacé la nausée.
