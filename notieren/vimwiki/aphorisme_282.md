# L’allure

## aus arasite.org/nietgaysci.html

Même les grands esprits peuvent révéler leurs humbles origines dans des choses
telles que la "démarche et la foulée" de leurs pensées (227).  L'écriture à long
terme peut être considérée comme une façon de tenter de le dissimuler.
