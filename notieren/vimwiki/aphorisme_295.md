# Brèves habitudes

## aus <https://www.reddit.com/r/Nietzsche/comments/gh1g9t/the_gay_science_life_as_an_experiment_1316/>

Nietzsche apprécie les habitudes brèves mais déteste les habitudes
durables. Les habitudes peuvent concerner la nourriture que nous
mangeons, les idées que nous avons, les relations que nous entretenons,
les arts que nous explorons, la façon dont nous organisons nos
journées ou le mode de vie que nous menons. Une habitude implique
une discipline et une structure, qui sont nécessaires pour vraiment
explorer les idées et les valeurs, mais les habitudes à long terme
sont étouffantes et tyranniques. L'absence d'habitudes est des
plus intolérables car il n'existe aucune structure ou base à partir
de laquelle on peut explorer et comparer des idées et des valeurs.

## aus arasite.org/nietgaysci.html

Les enthousiasmes brefs sont les meilleurs.  Nous avons toujours tendance à
penser qu'ils dureront, mais nous pouvons les abandonner s'ils nous rassasient ;
quelque chose de nouveau viendra bientôt.  Les habitudes durables peuvent être
tyranniques et sont à éviter.  Même la maladie [et la maniaco-dépression] peut
être bénéfique si elle nous aide à les éviter.  En même temps, certaines
habitudes sont cruciales, et personne ne peut vivre sur la base d'une
perpétuelle improvisation [une qualification importante de l'exhortation à vivre
dangereusement, dit Kaufmann. Plus d'autojustification, dit Harris].
