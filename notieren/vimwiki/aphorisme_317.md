# Coup d’œil rétrospectif

## aus arasite.org/nietgaysci.html

La vie est vraiment vécue à travers le pathos, bien que nous ne nous en rendions
souvent pas compte sur le moment et que nous pensions plutôt à poursuivre un
ethos.  Il continue d'avoir des souvenirs douloureux, et réalise que cela finira
par l'empêcher d'avoir une vie normale [une interprétation horriblement plate de
l'agonie du pauvre type].
