# trouver le maximum d'une liste


def maxListe(L):
    m = L[0]
    for i in range(1, len(L)):
        if L[i] > m:
            m = L[i]
    return m
