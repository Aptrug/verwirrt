# Ce qui appartient à la grandeur

## aus arasite.org/nietgaysci.html

Les grands hommes doivent se trouver capables d'infliger des souffrances.  Il ne
suffit pas d'être capable d'endurer la souffrance, car même les "femmes faibles
et même les esclaves" peuvent le faire avec succès.  Les grands, en revanche,
doivent maîtriser leurs propres doutes quant à leur capacité à infliger des
souffrances.
