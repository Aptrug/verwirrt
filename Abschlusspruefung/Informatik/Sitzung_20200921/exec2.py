def zeroDiagonal(A):
    k = 0
    for i in range(len(A)):
        A[i][k] = 0
        k += 1
    return A


M = [[1, 5, 3, 9], [4, 3, 5, 5], [8, 1, 8, 9], [7, 2, 4, 1]]
print(zeroDiagonal(M))
