# Deux hommes heureux

## aus arasite.org/nietgaysci.html

Les meilleurs d'entre nous peuvent improviser et prendre des risques tout en
paraissant infaillibles.  Ils sont ouverts à la contingence.  D'autres semblent
être enclins à l'accident, mais ils peuvent s'y réconcilier avec fatalisme,
conscients qu'ils ont eu à la fois des échecs et des succès, mais que même les
échecs peuvent les aider à mieux profiter de la vie [Kaufmann admet ouvertement
que c'est là une référence personnelle].
