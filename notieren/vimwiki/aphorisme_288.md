# Etats d’âme élevés

## aus arasite.org/nietgaysci.html

Le fait d'avoir des humeurs et des sentiments élevés nous rend vraiment humains.
De tels êtres humains peuvent éventuellement apparaître après "un grand nombre
de conditions préalables favorables", qui découlent de jets de dés plus que
chanceux.  Ces conditions peuvent prendre l'apparence d'un état permanent de
mouvement entre le haut et le bas [ !]
