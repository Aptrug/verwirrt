# quick-notes

## emacs

find which binding belongs to which command
c-h w

## awk is great

echo 'Lorem (ipsum) dolor (sit)' | awk 'BEGIN{ FS="[()]" } {print $2}'
output: `sit`

## "Disziplin ist alles!" — Helmuth von Moltke

Deutschland heil!

## forget what X means in [ -X "$var" ]

consult `man test`

## some X clipboard managing utilities

sxhkd, xbindkeys, xautomation (xte), xcape

## output exit code of the last executed command

`echo $?`

## Neovim autocomplete

`C-p`

## Lesson learned

`markdownlint` is unusable, since it doesn't provide any binaries or executable

## easy to package with nix

Go, Perl, Python, Lisp, Lua, Lua, Rust, Dune, Dotnet and Luarocks

## Access Vim history

`q:` or `q/`

## Append output of an external command to vim

:read !ls

## toAvoid

c++ (fucking stay away from it), perl, emacs, firefox
haskell (it's ungoogleable)

## select all match and replace

leader sip

## while seeking to use a good image file format

avoid jpeg, embrace png

## never runs these commands

1. `nix-collect-garbage --delete-old` will make `refresh` take forever

## languages to learn

<!--python, java and c++-->
python, javascript, regex (sed), rust and vimscript

## connect from termux to linux

ssh hajmdal@192.168.1.174

## check systemctl service

journalctl -b --user -u clipboard_cyka

## know nix package name

readlink -e $(which $APP)

## connect to WLAN

nmtui

## youtube-api

AIzaSyAsP8SbqQYgCBMq8l0_btxh6lDhrs8AjKM

## useful Reddit-like sites

<https://news.ycombinator.com/>

## useful chromium extensions

WhenX for Google Search \\
Free Email Tracker by cloudHQ

## useful German learning apps

gutefrage, hinative

## User and root mail location

`/var/spool/mail`

## useful apps

Fooview

## Dem Trottel Geld geben

spätestens am 15. eines jeden Monats

## Soap

Taous

## Vim command line

Either `shift+insert` or `<c-r>+`
use q: to navigate command line history and then copy

## Leeres Zeichen

[‏‏‎]

## markdownlint guide

<https://fga-eps-mds.github.io/2019.1-PyLearner/policies/Style-Guide/>

## convert .docx to .pdf with pandoc

`pandoc file.docx -o file.pdf`

## rclone mount remote

`rclone --vfs-cache-mode writes mount Aptrug: ~/Aptrug` \
`rclone --vfs-cache-mode full mount YS47: ~/YS47`

## search for a specific image size with Google Image

imagesize:1366x768

## Wallapop

Said S. \
<https://es.wallapop.com/user/saids-91661635>

## Preußische Tugendenliste

Aufrichtigkeit \
Bescheidenheit \
Disziplin \
Ehrlichkeit \
Fleiß \
Gehorsam \
Geradlinigkeit \
Gerechtigkeitssinn \
Gewissenhaftigkeit \
Gottesfurcht bei religiöser Toleranz \
Härte gegen sich mehr noch als gegen andere \
Mut \
Ordnungsliebe \
Ordnungssinn \
Pflichtbewusstsein \
Pflichterfüllung \
Pflichtgefühl \
Pünktlichkeit \
Redlichkeit \
Sauberkeit \
Selbstbewusstsein \
Selbstverleugnung \
Sparsamkeit \
Tapferkeit ohne Wehleidigkeit \
Toleranz \
Treue \
Unbestechlichkeit \
Unterordnung \
Weltoffenheit \
Zielstrebigkeit \
Zurückhaltung \
Zuverlässigkeit

## *Ipsum* --> \textit{Ipsum}

```text
(^|[^\*])\*([^\*]+)\*([^\*]|$)
$1\\textit{$2}$3
```

## installing evillimiter on Leap

`python3 -m site` \
`sudo python3 setup.py install --install-lib /usr/lib64/python3.8/site-packages`

## Mount shared_folder on Linux

`sudo mount -t vboxsf shared_folder /home/t440p/shared_folder/`

## Show dns in Huawei router site

old router

```text
$('#dhcp_dns_statistic').show();
$('#dhcp_primary_dns').show();
$('#dhcp_secondary_dns').show();
```

school's

```text
$('#dhcp_dns').show();
$('#dhcp_dns_checkbox_div').show();
$('#dhcp_dns_checkbox').show();
$('#dhcp_primary_dns_server_div').show();
$('#dhcp_primary_dns_server').show();
$('#dhcp_secondary_dns_server_div').show();
$('#dhcp_secondary_dns_server').show();
```

## More verbose systemd logs at boot / reboot

`vim /etc/default/grub` \
`update-grub == grub2-mkconfig -o /boot/grub2/grub.cfg`

## prevent windscribe dns conflict

`zypper install openresolv`

## Zentrum

32.350080, -6.343568

## der-die-das

<https://github.com/dtuggener/CharSplit> \
<https://github.com/aakhundov/deep-german> \
<https://github.com/gambolputty/german_nouns>

## Create multiple directories in every subdirectory

```bash
for dir in */;
do mkdir -- "$dir"/{Mathematik,Englisch,Uebersetzung};
done
```

## making cloudflared work

`/etc/cloudflared/config.yml` \
`sudo cloudflared service install` \
`127.0.0.1`

## cut video from start to end time

 ffmpeg -ss 00:01:00 -i input.mp4 -to 00:02:00 -c copy output.mp4

## remove several parts from video

```bash
ffmpeg -i 720p.h264.mp4 -filter_complex \
  "[0:v]trim=duration=446[av];[0:a]atrim=duration=446[aa];\
  [0:v]trim=start=506:end=570,setpts=PTS-STARTPTS[bv];\
  [0:a]atrim=start=506:end=570,asetpts=PTS-STARTPTS[ba];\
  [av][bv]concat[cv];[aa][ba]concat=v=0:a=1[ca];\
  [0:v]trim=start=606,setpts=PTS-STARTPTS[dv];\
  [0:a]atrim=start=606,asetpts=PTS-STARTPTS[da];\
  [cv][dv]concat[outv];[ca][da]concat=v=0:a=1[outa]" -map [outv] -map [outa] out.mp4
```

## list all environment eariables

### minimal

`env | nvim`

### detailed

`set | nvim`

### an alternative

`declare -px | nvim`
