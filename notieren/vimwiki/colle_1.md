# colle 1

## texte 1

*The death of Leopoldine, Hugo's daughter, drowned in 1843, divides
**les Contemplations** into two main parts: "Autrefois" and "Aujourd'hui".
Written in 1847, this poem allows Hugo to meet his missing child,
eternally present in his memory.*

Tomorrow, at dawn, at the moment when the land whitens, I will leave. You see, I
know that you are waiting for me. I will go through the forest, I will go across
mountains. I cannot stay away from you any longer.

I will walk eyes fixed on my thoughts, Without seeing anything outside, without
hearing a noise, Alone, unknown, back hunched, hands crossed, Sorrowed, as the
day for me is like the night.

I will watch neither the evening gold fall, Nor the faraway sails descending
upon Harfleur. And when I arrive, I will put on your grave A bouquet of green
holly and heather in bloom.

## texte 2

### French

Ainsi, à longueur de semaine, les prisonniers de la peste se
débattirent comme ils le purent. Et quelques-uns d’entre eux,
comme Rambert, arrivaient même à imaginer, on le voit, qu’ils
agissaient encore en hommes libres, qu’ils pouvaient encore
choisir. Mais, en fait, on pouvait dire à ce moment, au milieu du
mois d’août, que la peste avait tout recouvert. Il n’y avait plus
alors de destins individuels, mais une histoire collective qui
était la peste et des sentiments partagés par tous. Le plus grand
était la séparation et l’exil, avec ce que cela comportait de peur
et de révolte. Voilà pourquoi le narrateur croit qu’il convient, à
ce sommet de la chaleur et de la maladie, de décrire la situation
générale et, à titre d’exemple, les violences de nos concitoyens
vivants, les enterrements des défunts et la souffrance des
amants séparés.

C’est au milieu de cette année-là que le vent se leva et souffla
pendant plusieurs jours sur la cité empestée. Le vent est
particulièrement redouté des habitants d’Oran parce qu’il
ne rencontre aucun obstacle naturel sur le plateau où elle
est construite et qu’il s’engouffre ainsi dans les rues avec
toute sa violence. Après ces longs mois où pas une goutte d’eau
n’avait rafraîchi la ville, elle s’était couverte d’un enduit gris qui s’écailla
sous le souffle du vent. Ce dernier soulevait ainsi des vagues de
poussière et de papiers qui battaient les jambes des promeneurs
devenus plus rares. On les voyait se hâter par les rues, courbés
en avant, un mouchoir ou la main sur la bouche. Le soir, au lieu
des rassemblements où l’on tentait de prolonger le plus possible
ces jours dont chacun pouvait être le dernier, on rencontrait de
petits groupes de gens pressés de rentrer chez eux ou dans des
cafés, si bien que pendant quelques jours, au crépuscule qui
arrivait bien plus vite à cette époque, les rues étaient désertes et le
vent seul y poussait des plaintes continues. De la mer soulevée
et toujours invisible montait une odeur d’algues et de sel. Cette
ville déserte, blanchie de poussière, saturée d’odeurs marines,
toute sonore des cris du vent, gémissait alors comme une île
malheureuse.

Jusqu’ici la peste avait fait beaucoup plus de victimes dans
les quartiers extérieurs, plus peuplés et moins confortables, que
dans le centre de la ville. Mais elle sembla tout d’un coup se
rapprocher et s’installer aussi dans les quartiers d’affaires. Les
habitants accusaient le vent de transporter les germes
d’infection. « Il brouille les cartes », disait le directeur de l’hôtel.
Mais quoi qu’il en fût, les quartiers du centre savaient que leur
tour était venu en entendant vibrer tout près d’eux, dans la nuit,
et de plus en plus fréquemment, le timbre des ambulances qui
faisait résonner sous leurs fenêtres l’appel morne et sans passion de la peste.

À l’intérieur même de la ville, on eut l’idée d’isoler certains
quartiers particulièrement éprouvés et de n’autoriser à en sortir
que les hommes dont les services étaient indispensables. Ceux
qui y vivaient jusque-là ne purent s’empêcher de considérer
cette mesure comme une brimade spécialement dirigée contre
eux, et dans tous les cas, ils pensaient par contraste aux
habitants des autres quartiers comme à des hommes libres.
Ces derniers, en revanche, dans leurs moments difficiles, trouvaient
une consolation à imaginer que d’autres étaient encore moins
libres qu’eux. « Il y a toujours plus prisonnier que moi » était la
phrase qui résumait alors le seul espoir possible.

### English

*The Plague is first of all the chronicle of a struggle: that of the inhabitants
of Oran faced with the absurdity of their situation, locked up in a city where
the plague is becoming more and more frightening every day. In this excerpt, the
plague reaches its climax and no one thinks of denying its existence. From now
on, everyone must act, as they can, to ward off the disease.*

Thus week by week the prisoners of plague put up what fight they could. Some,
like Rambert, even contrived to fancy they were still behaving as free men and
had the power of choice. But actually it would have been truer to say that by
this time, mid-August, the plague had swallowed up everything and everyone. No
longer were there individual destinies; only a collective destiny, made of
plague and the emotions shared by all. Strongest of these emotions was the sense
of exile and of deprivation, with all the crosscurrents of revolt and fear set
up by these. That is why the narrator thinks this moment, registering the climax
of the summer heat and the disease, the best for describing, on general lines
and by way of illustration, the excesses of the living, burials of the dead, and
the plight of parted lovers.

It was at this time that a high wind rose and blew for several days through the
plague-stricken city. Wind is particularly dreaded by the inhabitants of Oran,
since the plateau on which the town is built presents no natural obstacle, and
it can sweep our streets with unimpeded violence. During the months when not a
drop of rain had refreshed the town, a gray crust had formed on everything, and
this flaked off under the wind, disintegrating into dust-clouds. What with the
dust and scraps of paper whirled against people’s legs, the streets grew
emptier. Those few who went out could be seen hurrying along, bent forward, with
handkerchiefs or their hands pressed to their mouths. At nightfall, instead of
the usual throng of people, each trying to prolong a day that might well be his
last, you met only small groups hastening home or to a favorite café. With the
result that for several days when twilight came it fell much quicker at this
time of the year—the streets were almost empty, and silent but for the long
drawn stridence of the wind. A smell of brine and seaweed came from the unseen,
storm-tossed sea. And in the growing darkness the almost empty town, palled in
dust, swept by bitter sea-spray, and loud with the shrilling of the wind, seemed
a lost island of the damned.

Hitherto the plague had found far more victims in the more thickly populated and
less well-appointed outer districts than in the heart of the town. Quite
suddenly, however, it launched a new attack and established itself in the
business center. Residents accused the wind of carrying infection, "broadcasting
germs," as the hotel manager put it. Whatever the reason might be, people living
in the central districts realized that their turn had come when each night they
heard oftener and oftener the ambulances clanging past, sounding the plague’s
dismal, passionless tocsin under their windows.

The authorities had the idea of segregating certain particularly affected
central areas and permitting only those whose services were indispensable to
cross the cordon. Dwellers in these districts could not help regarding these
regulations as a sort of taboo specially directed at themselves, and thus they
came, by contrast, to envy residents in other areas their freedom. And the
latter, to cheer themselves up in despondent moments, fell to picturing the lot
of those others less free than themselves. "Anyhow, there are some worse off
than I," was a remark that voiced the only solace to be had in those days.

## texte 3

*Jean Giono creates the character of Angelo Pardi, brilliant rider, sparkling
fencer, quick to be indignant in front of the lowesses. Fleeing the cholera, the
young man befriends Pauline de Théus. This one, however, is in its turn reached
by the disease.*

The night had become extremely dark and silent.

“This isn’t the first time,” thought Angelo, “but they’ve all died in my hands.”

The absence of hope, rather than despair, and above all physical exhaustion now
made him more and more frequently turn to gaze into the night. He was not
seeking help but some repose.

Pauline seemed to be slipping away. He dared not question her. The words of the
man in the riding-coat were still too recent. He remembered the lucidity of
which the man had spoken, and he dreaded lucidity from this mouth, still
discharging its whitish mud.

He was amazed, even rather terrified, by the emptiness of the night. He wondered
how he had managed not to be frightened till now, especially of things so
menacing. Yet he never ceased to labor with his hands to bring back warmth to
that groin at the edges of which the cold and marble hue still lurked.

At length a whole series of little, highly colored, brightly lit thoughts came
to him, some of them absurd and laughable, and, at the end of his tether, he
rested his cheek on that stomach, now shuddering only feebly, and fell asleep. A
pain in his eyes woke him; he saw red, opened his eyes. It was day.

He could not think what the soft, warm thing was on which his head was resting.
He could see he was covered to the chin by the folds of his cloak. He breathed
deeply. A cool hand touched his cheek.

“I covered you up,” said a voice. “You were cold.”

He was on his feet in an instant. The voice was not entirely unfamiliar. Pauline
was looking at him with almost human eyes.
