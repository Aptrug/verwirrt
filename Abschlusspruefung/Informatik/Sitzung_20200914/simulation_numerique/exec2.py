import numpy as np

import pylab as plb

question = float(input('Question: '))
if question == 1.:
    t = np.linspace(0, 2 * np.pi, 200)
    x = np.sin(t) / (1 + np.cos(t) ** 2)
    y = np.sin(t) * np.cos(t) / (1 + np.cos(t) ** 2)
    plb.plot(x, y)
    plb.show()
elif question == 2.:
    t = np.linspace(0, 10 * np.pi, 200)
    x = t * np.cos(t)
    y = t * np.sin(t)
    plb.plot(x, y)
    plb.show()
elif question == 3.:
    t = np.linspace(0, 2 * np.pi, 200)
    x = 16 * np.sin(t)**3
    y = 13 * np.cos(t) - 5*np.cos(2*t) - 2*np.cos(3*t) - np.cos(4*t)
    plb.plot(x, y)
    plb.show()
elif question == 4.:
    p = 11
    q = 7
    t = np.linspace(0, 2 * np.pi * q, 2000)
    x = (1 + np.cos((p/q)*t))*np.cos(t)
    y = (1 + np.cos((p/q)*t))*np.sin(t)
    plb.plot(x, y)
    plb.show()
