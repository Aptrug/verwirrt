# tester si la liste contient des éléments consécutifs

L = []
for i in range(10):
    x = int(input("Donner un valeur: "))
    L.append(x)
test = True
i = 0
while i < 9 and test is True:
    if L[i] != L[i + 1] - 1:
        test = False
    i += 1
if test is False:
    print(False)
else:
    print(True)
