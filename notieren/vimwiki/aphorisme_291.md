# Gênes

## aus arasite.org/nietgaysci.html

La ville de Gênes nous rappelle qu'il y avait autrefois des êtres humains
audacieux et autocratiques qui étaient bien disposés envers la vie et se
contentaient de s'imposer dans le paysage local, voire de se faire concurrence
entre eux.  Les villes modernes d'Europe du Nord sont différentes, ce qui
suggère que les gens doivent se considérer comme égaux.  Les gens qui ont
construit Gênes en savent beaucoup plus sur la nature et l'aventure et sur la
satisfaction d'imposer leur propre goût.
