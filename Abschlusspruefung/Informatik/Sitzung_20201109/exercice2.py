def nombreNoeuds(arb):
    if arb == []:
        return 0
    return 1 + nombreNoeuds(arb[1]) + nombreNoeuds(arb[2])


# test
arb = [15, [7, [6, [], []], [9, [], []]], [20, [], [25, [], []]]]
print(nombreNoeuds(arb))
