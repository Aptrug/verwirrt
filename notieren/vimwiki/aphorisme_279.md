# Amitié d’astres

## aus arasite.org/nietgaysci.html

On se fait des amis et on les perd, mais c'est tout à fait normal, et au moins
le souvenir des amitiés en général peut être valorisé, quelle que soit la
relation qui s'avère être en pratique.
