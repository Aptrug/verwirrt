# Faire du tort à la bêtise

## aus arasite.org/nietgaysci.html

Prêcher que l'égoïsme est répréhensible n'a fait que nuire à l'ego au détriment
du troupeau.  L'égoïsme débridé est considéré comme la source du malheur.
Cependant, pour d'autres, l'obéissance irréfléchie et stupide aux règles était
la raison du malheur, et la pensée produit le plus grand bonheur.  Cette
critique privait la stupidité de sa bonne conscience.
