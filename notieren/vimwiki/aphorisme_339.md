# Vita femina

## aus arasite.org/nietgaysci.html

La connaissance ou la bonne volonté ne nous aideront pas à voir la beauté
ultime.  Nous avons besoin de chance et d'accident également, en nous tenant
exactement au bon endroit [métaphore du paysage].  Il se peut que nous n'ayons
qu'un seul aperçu.  C'est ce qui rend la vie si merveilleuse.  C'est pourquoi
"la vie est une femme"(272).
