# Préludes à la science

## aus arasite.org/nietgaysci.html

La science n'a vu le jour qu'après de nombreux travaux préalables de magiciens
et d'astrologues, qui ont d'abord fait allusion à la possibilité de pouvoirs
cachés.  Même la religion peut être considérée comme un prélude à la
connaissance de cette manière, faisant au moins allusion à un certain ensemble
sous-jacent ou au pouvoir de rédemption de soi.  La formation religieuse peut
également avoir pour conséquence involontaire de libérer les gens de l'idée du
créateur.
