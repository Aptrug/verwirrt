# Les médecins de l’âme et la douleur

## aus arasite.org/nietgaysci.html

Les théologiens ont essayé de nous faire croire que nous sommes si mauvais que
nous devons avoir un remède radical, et cela produit une négativité collective.
Dans le même temps, les gens ont une ingéniosité infinie lorsqu'il s'agit de
faire face aux malheurs.  Il existe néanmoins une convention qui consiste à
exagérer la douleur et le malheur, et non la façon dont on les traite.  Les
religieux se contentent de mentir sur le malheur des gens, alors qu'ils savent à
quel point cette volonté et cette passion contribuent au bonheur.  En pratique,
peu d'entre nous aimeraient mener une vie stoïque.
