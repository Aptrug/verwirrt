# Loisir et oisiveté

## aus arasite.org/nietgaysci.html

La soif d'or des Américains a commencé à infecter l'Europe en dédaignant la
spiritualité, ou même en se reposant et en réfléchissant.  Une activité sans fin
menace la culture et le goût, et nous pouvons le constater dans "la demande
universelle d'évidence brute" (259).  Il n'y a pas de temps pour la grâce ni
même pour les loisirs.  La vertu devient une question de faire quelque chose en
moins de temps.  Le travail se termine par la fatigue.  On peut le constater
dans la façon dont les gens écrivent des lettres.  Tout plaisir tiré des arts
reste une forme d'ivresse.  Le travail est au centre de la bonne conscience.
Les loisirs et la joie sont considérés comme une simple récupération.  Il est
désormais presque impossible de se promener "avec des idées et des amis" (260).
Auparavant, c'était le contraire, la noblesse et l'honneur s'exprimaient dans
les loisirs ou la guerre, mais jamais dans le travail
