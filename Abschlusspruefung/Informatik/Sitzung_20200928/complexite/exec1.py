# 1.
def factorielle(n):
    k = 1
    for i in range(2, n+1):
        k *= i
    return k


# 2
# a)
print('mystere(4) renvoie s = 1! + 2! + 3! + 4! = 33')
# b)
print('soit f(n) le nombre de multiplication')
print('f(n) = 1 + 2 + 3 ... + (n-1) = n*(n-1)/2')


# c)
def mystere(n):
    s = 0
    k = 0
    for i in range(2, n+1):
        k = k*i
        s = s + k
    return s
