# Notre air

## aus arasite.org/nietgaysci.html

La rigueur de la science fait craindre à beaucoup de gens que ce soit trop
difficile, mais la sévérité de la science produit une certaine pureté virile, où
il n'est pas nécessaire de tergiverser ou d'être prudent.  Il est difficile,
mais nécessaire d'"apporter la lumière à la terre" (236) avec rapidité et
sévérité, d'être viril et terrible.  Fainthearts n'approuvera pas. [Il se prend
pour un scientifique ? Le déterminisme physiologique est ce qu'il a en tête ?]
