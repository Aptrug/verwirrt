# Lumière réfractée

## aus arasite.org/nietgaysci.html

Parfois, nous nous sentons fatigués de devoir vivre dans l'isolement et d'éviter
de nous offenser.  Nous pensons parfois qu'il est plus simple d'être stupide
comme tous les autres.  Il y a du plaisir à ce que d'autres personnes se
dressent contre vous, mais sa propre position consiste à exposer tout ce qui est
faux, les gaffes, les confusions et les contradictions, et à laisser les gens du
commun se moquer de lui s'il le faut.  Il aurait peut-être été préférable
autrefois de célébrer les grands créateurs, mais personne n'est indispensable de
nos jours.  Cependant, nous devons savourer nos propres moments d'audace
[Kaufmann dit que c'est une bonne indication de la personnalité de Nietzsche, où
il désespère de ne pas pouvoir communiquer avec les gens ordinaires].
