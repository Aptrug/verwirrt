# trouver l'indice du minimum d'une liste


def indiceMin(L):
    m = 0
    for i in range(1, len(L)):
        if L[i] < L[m]:
            m = i
    return m


print(indiceMin([1, 2, 3]))
