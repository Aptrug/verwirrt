# La pensée de la mort

## aus <https://www.reddit.com/r/Nietzsche/comments/gqjm37/the_gay_science_the_value_of_life_1516/>

Nietzsche est heureux que les gens soient tellement immergés dans
la vie qu'ils ne pensent pas à la mort ; en fait, il veut rendre
la pensée de la vie encore plus attrayante. Les gens ne devraient
pas se précipiter dans la vie en vivant toujours pour le futur
proche car la mort les attend (remarquez qu'il ne mentionne pas
ici la récurrence éternelle).

## aus arasite.org/nietgaysci.html

L'agitation de la vie indique la jouissance et le désir, bien que l'ombre de la
mort plane toujours sur elle.  Il est tentant de considérer la vie comme une
préparation à l'avenir, alors que la certitude de la mort semble n'avoir aucun
impact.  C'est une bonne chose, et l'objectif de Nietzsche est de rendre la vie
aussi attrayante que possible dans l'esprit des autres.
