def sommeMat(A, B):
    C = [[0 for i in range(len(A[0]))] for j in range(len(A))]
    for i in range(len(A)):
        for j in range(len(A[0])):
            C[i][j] = A[i][j] + B[i][j]
    return C


M = [[93, 17, 64, 75], [64, 81, 92, 3], [61, 57, 33, 9], [81, 50, 74, 47]]
N = [[51, 91, 3, 54], [5, 17, 14, 67], [35, 24, 86, 43], [74, 22, 93, 37]]
C = sommeMat(M, N)

print(C)
