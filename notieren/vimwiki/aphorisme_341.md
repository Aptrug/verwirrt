# Le poids le plus lourd

## aus <https://www.reddit.com/r/Nietzsche/comments/gqjm37/the_gay_science_the_value_of_life_1516/>

Nietzsche présente l'idée de la récurrence éternelle comme une
expérience de pensée. Le passage commence en supposant qu'un démon
vous révèle que la récurrence éternelle est vraie - que la vie
que vous vivez et avez vécue de nombreuses fois auparavant se répètera
éternellement avec tous les mêmes détails. Selon l'expérience de
pensée de Nietzsche, le cosmos se répète éternellement, donc chacun
de nous a déjà vécu sa vie de nombreuses fois, et nous la vivrons
encore de nombreuses fois ; nos vies nous semblent à chaque fois
nouvelles, mais les détails, bons et mauvais, sont toujours les
mêmes.

La récurrence éternelle implique que la vie n'a pas de sens, de valeur
ou de but intrinsèque. C'est une forme de nihilisme, et Nietzsche
se préoccupe de l'apesanteur du nihilisme - que rien ne nous fonde
dans la vie. Une autre implication est qu'il n'y a rien d'autre
que la vie ; il n'y a pas de vie après la mort à laquelle nous
nous préparons. La question que pose Nietzsche est de savoir si
l'on désire suffisamment la vie elle-même pour vouloir la revivre
d'innombrables fois. Elle est censée peser le plus lourd dans nos
actions car si nous pouvons nous affirmer joyeusement et affirmer
nos actions, alors nous nous ancrons dans la vie et évitons ainsi
le nihilisme. Cette expérience de réflexion est donc un test :
si vous appreniez que la récurrence éternelle est vraie, comment
réagiriez-vous ? Seriez-vous capable de vous affirmer joyeusement
et d'affirmer votre vie telle qu'elle est ? Seriez-vous capable
d'affirmer joyeusement la vie s'il n'y a que la vie, pas d'après-vie
? Ou êtes-vous aussi un pessimiste, comme Socrate ?

Il ne s'agit pas de nous motiver à commencer à vivre mieux en prétendant
que nous aurons besoin de vivre cette vie de manière répétée. Lorsque
Nietzsche dit : "La question qui se pose en toute chose, "Désirez-vous
ceci une fois de plus et d'innombrables fois encore ? serait le
poids le plus lourd de vos actes", il ne veut pas dire que nous
pouvons nous poser cette question avant d'agir, comme une sorte
de motivation, et décider ensuite de la meilleure façon d'agir
; nous devrions plutôt nous poser cette question au fur et à mesure
que les choses se déroulent pour voir si nous pouvons nous affirmer
joyeusement et affirmer notre vie telle qu'elle est - pouvons-nous
aimer notre destin ? (voir n° 276). D'où sa question suivante :
"dans quelle mesure devriez-vous être bien disposé envers vous-même
et envers la vie" - c'est-à-dire dans quelle mesure devriez-vous
être à l'aise avec vous-même et avec votre vie telle qu'elle est,
bonne et mauvaise, pour vouloir la refaire sans cesse ? De plus,
le postulat est que nous avons déjà vécu notre vie d'innombrables
fois auparavant avec les mêmes détails.

Le fait qu'un démon nous révèle l'idée d'une récurrence éternelle
n'a pas pour but d'évoquer la mythologie chrétienne. Étant donné
le thème commun de 276 et 341, il semble que Nietzsche fasse à
nouveau référence à Descartes, qui a proposé une expérience de
pensée différente qui supposait que tout ce que nous vivons est
une illusion parce qu'un démon nous a trompés (à l'origine, Descartes
supposait que Dieu nous trompait, mais cela a été exclu).

Nietzsche a sérieusement envisagé l'idée de la récurrence éternelle pendant
une courte période, mais il n'y a rien dans les ouvrages publiés
de Nietzsche qui propose la récurrence éternelle comme une thèse
sérieuse sur la façon dont le monde est réellement. L'idée est
également explorée dans le prochain livre de Nietzsche, Ainsi parlait
Zarathoustra, mais le but est aussi d'explorer l'idée de savoir
si Zarathoustra peut affirmer la vie une fois que la vie elle-même
lui dit que la récurrence éternelle est vraie. Le passage suivant
(n° 342) nous présente Zarathoustra.

## aus arasite.org/nietgaysci.html

 L'idée de la récurrence éternelle peut être effrayante si nous pensons que nous
 devons simplement revivre la vie avec "rien de nouveau", avec toutes les
 douleurs et les joies. Elle semble si indifférente aux individus.  Cependant,
 une telle possibilité peut aussi être considérée comme divine, car elle
 aiderait à guider nos actions actuelles.  Nous savons que nous avons fait du
 bon travail envers la vie et envers nous-mêmes si nous "n'aspirons à rien de
 plus ardemment que cette ultime confirmation et ce sceau éternel" (274).
