# Incipit tragoedia

## aus <https://www.reddit.com/r/Nietzsche/comments/glflki/the_gay_science_monotheism_polytheism_and_overmen/>

Ce passage était le dernier de The Gay Science jusqu'à ce que Nietzsche
ajoute le livre V en 1887. Il est presque identique au premier
passage du livre suivant de Nietzsche, Ainsi parlait Zarathoustra.
Le Zarathoustra de Nietzsche est inspiré de Zarathoustra, ou Zoroastre,
le fondateur du zoroastrisme (l'histoire réelle est obscure). Le
zoroastrisme est une ancienne religion persane qui prêche la dualité
du bien et du mal, et qui a eu une influence sur le judaïsme primitif
(et donc indirectement sur le christianisme). Ainsi, Nietzsche
considère le Zarathoustra historique comme le premier prédicateur
de la dualité cosmique entre le bien et le mal. Dans Thus Spoke
Zarathustra, Nietzsche s'approprie le personnage historique Zarathustra
à ses propres fins ; il réinterprète Zarathustra comme un prophète
pour l'Ubermensch (Overman) - un personnage idéal qui dépasse l'homme.
Le Zarathoustra de Nietzsche prêchait le dépassement de la dualité
du bien et du mal. C'est une façon pour Nietzsche d'explorer et
de préconiser une autre façon d'être, sans pour autant suggérer
qu'une telle figure est littéralement réalisable. En d'autres termes,
Nietzsche crée un nouvel exemple de figure idéale, similaire à
la façon dont les Grecs ont créé les figures d'Achille, d'Athéna,
d'Aphrodite et autres, et les Juifs ont créé Abraham, Moïse, Jésus
et autres (une différence est que Nietzsche est conscient de ce
processus créatif, ce qui n'était généralement pas le cas pour
d'autres histoires mythiques). Les histoires sur Zarathoustra et
Overman sont destinées à guider nos vies, tout comme les histoires
sur Achille et Aphrodite ont guidé la vie des Grecs, et les histoires
sur Abraham et Jésus guident la vie des chrétiens. Nietzsche propose
un nouvel idéal à la suite de la mort de Dieu. Il s'approprie même
le style des textes judéo-chrétiens, c'est pourquoi "Ainsi parlait
Zarathoustra" se lit comme la Bible.

Le titre de ce passage est également un indice que Nietzsche participe
consciemment à la tradition de création de figures idéales mais
irréelles pour guider nos vies. Le titre de ce dernier passage
(original) est Incipit Tragoedia, ou la tragédie commence. Dans
le premier passage de The Gay Science, "Les maîtres du but de l'existence",
Nietzsche critique ceux qui traitent la vie comme une tragédie/un
drame ; il suggère plutôt que la vie est une comédie. Ainsi, en
qualifiant l'histoire de Zarathoustra de tragique/drame, Nietzsche
fait, en un sens, ce pour quoi il critique les autres, mais il
n'est pas hypocrite car il remet également en cause cette tradition
en se l'appropriant. La structure de Thus Spoke Zarathustra correspond
au moule, mais le message remet en question les enseignements conventionnels,
en particulier les enseignements judéo-chrétiens qui ont eu tant
d'influence dans la culture de Nietzsche.

Un dernier indice que Nietzsche participe à la tradition de création
de figures idéales mais irréelles pour guider nos vies est que
le concept de l'Overman ne figure pas dans les œuvres de Nietzsche
avant ou après - c'est une expression créative de la philosophie
de Nietzsche. Si Nietzsche encourageait littéralement l'humanité
à créer de véritables Overmen, on pourrait penser qu'il aurait
approfondi cette idée dans des œuvres ultérieures (bien sûr, il
considérait les humains comme des animaux qui continueraient à
évoluer, il a donc compris que notre état d'être actuel n'est pas
une fin en soi). Au lieu de cela, le concept d'Overman est laissé
vague, ce qui, je pense, est intentionnel afin que les individus
puissent le remplir pour eux-mêmes - du moins les individus capables
d'imaginer ce que serait le fait d'être au-delà de l'homme.

## aus arasite.org/nietgaysci.html

Bien que Zarathoustra jouisse de la solitude, il s'est rendu compte qu'il avait
besoin que les gens reçoivent sa sagesse, même si cela impliquait de retourner
vivre parmi eux, de retourner aux enfers.
