# Nouvelle prudence

## aus arasite.org/nietgaysci.html

Au lieu d'essayer de changer les autres, nous devrions accepter que d'autres
facteurs soient beaucoup plus influents.  Nous devrions également changer les
gens uniquement en démontrant notre propre intelligence.
