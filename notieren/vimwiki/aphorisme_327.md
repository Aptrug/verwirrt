# Prendre au sérieux

## aus arasite.org/nietgaysci.html

Pour la grande majorité, il est difficile de commencer à réfléchir sans devenir
sérieux et pesant, mais la réflexion peut aussi s'accompagner de rires et de
gaieté [Kaufmann dit que c'est cela la science gay, et que Nietzsche lui-même
n'était pas particulièrement grave ou lugubre, bien que sa sœur ait fait croire
qu'il l'était.  Il sauve également Nietzsche contre l'accusation qu'il est
opposé à la science d'une manière ou d'une autre].
