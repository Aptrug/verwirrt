# Excelsior

## aus arasite.org/nietgaysci.html

[Apparemment, dit Kaufmann, ce passage fait référence à la discussion de l'homme
du renoncement, mais cette fois-ci avec une tournure positive].  Nous devrions
abandonner toute tentative de prier, de faire confiance, de restreindre nos
pensées ou de chercher quelqu'un qui nous rendra enfin la vie meilleure.  Il n'y
a pas de raison à ce qui se passe, pas d'amour éternel ni de lieu de repos.
Nous "voulons la récurrence éternelle de la guerre et de la paix" [la première
mention, dit Kaufmann] (230).
