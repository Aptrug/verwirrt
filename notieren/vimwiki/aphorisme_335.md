# Vive la physique

## aus <https://www.reddit.com/r/Nietzsche/comments/gcugt5/the_gay_science_reevaluation_of_values_1116/>

Les gens considèrent généralement que la voix de leur propre conscience
détermine à juste titre ce qui est moral, mais Nietzsche nous demande
pourquoi nous écoutons notre conscience et comment nous l'écoutons.
Concernant les raisons pour lesquelles nous l'écoutons, il explique
que notre conscience a une histoire de développement dans nos instincts,
nos préférences et nos expériences. En ce qui concerne la manière
dont nous l'écoutons, il demande si nous avons peur de l'ignorer,
si nous l'admirons ou l'aimons, si nous suivons les ordres comme
un bon soldat, si c'est parce que les autres nous félicitent de
la suivre, etc. Nietzsche veut que nous ayons une conscience intellectuelle
(#2), que nous réfléchissions à notre conscience - avoir une conviction
ferme dans sa conscience n'est pas nécessairement une chose noble,
car cela peut être un signe de faiblesse ou d'entêtement. Il fait
valoir que si nous comprenions mieux notre propre conscience, nous
ne serions pas aussi résolus et nous ne la tiendrions pas en si
haute estime. Tout ce que nous pouvons faire, c'est nous concentrer
sur la purification de nos propres opinions et valeurs et sur la
création de nos propres nouvelles tables de ce qui est bon et mauvais
(mais, rien n'est créé à partir de rien). Nous ne devrions pas
nous inquiéter de savoir si nos actions sont considérées comme
"morales" par ceux qui veulent préserver les valeurs du passé.
Nous voulons devenir les êtres uniques que nous sommes - des gens
qui se donnent des lois et qui se créent eux-mêmes. À cette fin,
nous devons apprendre tout ce qui est légal et nécessaire dans
ce monde ; nous devons devenir des physiciens en quelque sorte
pour connaître les nécessités et les caractéristiques de ce avec
quoi nous travaillons pour nous créer nous-mêmes. Nous ne pouvons
pas nous créer à partir de rien ; nous devons travailler avec ce
qui est et doit être (ce qui ne veut pas dire que tout se passe
nécessairement).

## aus arasite.org/nietgaysci.html

La physique nous a appris à observer les choses de manière non personnelle.  Se
connaître soi-même ou s'observer soi-même seulement est loin d'être essentiel
aux humains et à leur moralité.  Il est absurde de prétendre que nous sommes les
seuls à pouvoir répondre à des questions sur nous-mêmes, ou à y répondre avec
sagesse.  Il n'est certainement pas vrai que le fait de décider de ce qui est
bon pour soi soit la source d'une action morale, même si celle-ci obéit à ce que
nous dit la conscience : la conscience peut ne pas être vraie ou infaillible.
Qu'en est-il de la "conscience intellectuelle" ?  (263).  Les jugements
instinctifs de ce qui pourrait être juste ont une préhistoire, et nous devons
chercher comment ils en sont venus à paraître si corrects.  S'agissait-il d'un
ordre passé, d'une peur ou d'une stupidité ?  Prendre conscience de ce qui est
crucial signifie que nos opinions et les effets de leur éducation n'ont pas fait
l'objet d'une réflexion suffisante.  La fermeté avec laquelle les jugements
moraux sont portés peut être simplement l'entêtement ou l'incapacité à penser à
quelque chose de nouveau.  Une fois que nous comprenons comment un jugement
moral est né, nous ne pouvons pas le considérer comme sacré, et il en va de même
pour les concepts de péché ou de salut.  Quant à l'impératif catégorique, il
s'est simplement glissé dans la philosophie de Kant pour le domestiquer après
qu'il ait fait les premières avancées critiques.  Son aspect inconditionnel
reflète l'égoïsme.  Il est absurde de faire l'expérience de son propre jugement
comme une loi universelle, et cela limite même son propre moi.  La position de
chacun n'est pas la même, donc aucun jugement ne peut être collectif.  Les
circonstances extérieures domineront les sentiments intérieurs.  Il peut y avoir
une certaine similitude entre les opinions et les règles d'action, mais ce n'est
pas une preuve de la vérité, d'autant plus que de nombreuses actions ne sont pas
du tout examinées.  Ce que nous devrions faire, c'est réfléchir à nos propres
opinions et évaluations, en essayant de trouver une voie vers le bien, mais sans
nous préoccuper de la valeur morale [comme dans le domaine social ?] de ces
actions.  Nous ne devrions pas nous asseoir sur un jugement moral, en
reproduisant sans cesse le passé.  Nous devrions agir davantage comme des
physiciens afin d'indiquer la voie vers un nouvel ensemble de lois, en tenant
pleinement compte des facteurs qui ont produit nos expériences dans le passé.
