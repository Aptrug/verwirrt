# Pour la nouvelle année

## aus <https://www.reddit.com/r/Nietzsche/comments/gqjm37/the_gay_science_the_value_of_life_1516/>

Nietzsche propose sa résolution du nouvel an 1882 : il veut apprendre
à voir comme beau ce qui est nécessaire dans les choses, afin de
faire partie de ceux qui rendent les choses belles - Amor fati
(l'amour du destin). Ce point se rapporte au numéro 299 (partie
12 de cette série) où Nietzsche dit que nous devrions apprendre
des artistes comment rendre les choses belles en les voyant comme
belles. Nietzsche ne dit pas que toutes les choses arrivent nécessairement
(si toutes les choses arrivent nécessairement, alors sa propre
attitude à l'égard de la vie arrivera nécessairement et sa résolution
sera creuse). Mais, de nombreux aspects de la vie se produisent
nécessairement (il ne pourrait en être autrement) et beaucoup d'autres
choses qui se produisent sont hors de notre contrôle - la seule
chose que nous pouvons contrôler est notre attitude à leur égard.
La résolution de Nietzsche est non seulement d'embrasser ce qui
est condamné dans la vie, mais aussi de le considérer comme beau.
Il ne sera pas critique ou antagoniste ; sa seule négation ou négation
sera de détourner le regard, mais il veut travailler pour devenir
un "Yes-sayer" - pour affirmer tous les aspects de la vie.

Descartes dit "Je pense, donc je suis", alors que Nietzsche dit "Je suis,
donc je pense". Le point de vue de Descartes était qu'il est certain
qu'il sort parce qu'il pense. Nietzsche, lui, ne peut s'empêcher
de penser parce qu'il existe.

## aus arasite.org/nietgaysci.html

Il se sent toujours vivant et donc capable de penser à l'avenir.
Il se consacre à la fois à l'affirmation et à l'amor fati.  Il
dit qu'il ne veut plus accuser personne ni faire la guerre - il
se contente de détourner le regard.
