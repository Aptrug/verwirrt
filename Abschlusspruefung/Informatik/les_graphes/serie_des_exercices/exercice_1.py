# Exercice 1

# question 1

# #   1   2   3   4
# 1   0   1   1   0
# 2   1   0   0   1
# 3   1   0   0   0
# 4   0   1   0   0

# ___    ___    ___    ___
# |3|----|1|----|2|----|4|
# ^^^    ^^^    ^^^    ^^^

# question 2
def nbreSommet(G):
    return len(G)


# question 3
def adjacence(G):
    n = len(G)
    for i in range(n):
        if len(G[i]) != n:
            return False
        if G[i][i] != 0:
            return False
        for j in range(i):
            if G[i][j] != G[j][i]:
                return False
    return True


# testing
a = [[0, 1, 1, 0], [1, 0, 0, 1], [1, 0, 0, 0], [0, 1, 0, 0]]
print("le nombre de sommets:", nbreSommet(a))
print("matrice d'adjacence:", adjacence(a))
