# Exercice 2

soit $f(n)$ le nombre d'opérations élémentaires \
$$
f(n) = 1 + 2n + 3(1 + 2 + ... + n - 1) = 1 + 2n + 3 \dfrac{n(n-1)}{2} = O(n^2)
$$
