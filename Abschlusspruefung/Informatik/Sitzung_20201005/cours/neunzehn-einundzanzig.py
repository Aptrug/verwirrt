def triRapide(T, premier, dernier):
    if premier < dernier:
        indPivot = partitionner(T, premier, dernier)
        triRapide(T, premier, indPivot-1)
        triRapide(T, indPivot+1, dernier)


def partitionner(T, premier, dernier):
    j = premier
    for i in range(premier, dernier):
        if T[i] <= T[dernier]:
            T[i], T[j] = T[j], T[i]
            j = j + 1
    T[dernier], T[j] = T[j], T[dernier]
    return j

T = [23, 15, 56, 4, 89, 23, 14]
print("Avant le tri: ", T)
triRapide(T, 0, 6)
print("Après le tri: ", T)
