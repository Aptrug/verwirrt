# Du fond de la septième solitude

## aus arasite.org/nietgaysci.html

Les personnes qui recherchent la vérité et la réalité peuvent parfois désespérer
d'être poussées dans leur quête.  Mais ils se rendent vite compte que le
contentement et la beauté ne suffisent jamais à les retenir vraiment.
