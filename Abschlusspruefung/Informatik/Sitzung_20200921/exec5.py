def produitMat(A, B):
    C = [[0 for i in range(len(B[0]))] for j in range(len(A))]
    for i in range(len(C)):
        for j in range(len(C[0])):
            for k in range(len(B)):
                C[i][j] = C[i][j] + A[i][k] * B[k][j]
    return C


M = [[43, 24, 32, 43], [24, 32, 42, 14], [32, 52, 74, 82], [95, 29, 84, 72]]
N = [[3, 11, 92, 7], [90, 87, 49, 18], [55, 82, 39, 72], [77, 47, 81, 60]]
C = produitMat(M, N)
print(C)
