# reconnaître une chaîne de caractères palindrome avec python


def estpalindrome(ch):
    i = 0
    j = len(ch) - 1
    while i < j:
        if ch[i] != ch[j]:
            return False
        i += 1
        j -= 1
    return True


chaine = str(input('Donner la chaine: '))
if estpalindrome(chaine) is True:
    print('La chaine est palindrome')
else:
    print("La chaine n'est pas palindrome")
