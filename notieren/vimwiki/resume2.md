Les philosophes qui ont spéculé sur la signification de la vie et sur la
destinée de l’homme n’ont pas assez remarqué que la nature a pris la peine de
nous renseigner là-dessus elle-même. Elle nous avertit par un signe précis que
notre destination est atteinte. Ce signe est la joie. Je dis la joie, je ne dis
pas le plaisir. Le plaisir n’est qu’un artifice imaginé par la nature pour
obtenir de l’être vivant la conservation de la vie ; il n’indique pas la
direction où la vie est lancée. Mais la joie annonce toujours que la vie a
réussi, qu’elle a gagné du terrain, qu’elle a remporté une victoire: toute
grande joie a un accent triomphal. Or, si nous tenons compte de cette
indication et si nous suivons cette nouvelle ligne de faits, nous trouvons que
partout où il y a joie, il y a création: plus riche est la création, plus
profonde est la joie [...]. Si donc, dans tous les domaines, le triomphe de la
vie est la création, ne devons-nous pas supposer que la vie humaine a sa raison
d’être dans une création qui peut, à la différence de celle de l’artiste et du
savant, se poursuivre à tout moment chez tous les hommes: la création de soi
par soi, l’agrandissement de la personnalité par un effort qui tire beaucoup de
peu, quelque chose de rien, et ajoute sans cesse à ce qu’il y avait de richesse
dans le monde?

Philosophers who have speculated on the meaning of life and the destiny of man
have not noticed enough that nature has taken the trouble to inform us about
this itself. She warns us with a clear sign that our destination has been
reached. This sign is joy. I say joy, not pleasure. Pleasure is only an
artifice imagined by nature to obtain from the living being the conservation of
life; it does not indicate the direction in which life is launched. But joy
always announces that life has succeeded, that it has gained ground, that it
has won a victory: every great joy has a triumphal accent. If we take this
indication into account and follow this new line of facts, we find that
wherever there is joy, there is creation: the richer the creation, the deeper
the joy. If, then, in all fields, the triumph of life is creation, should we
not assume that human life has its raison d'être in a creation which can,
unlike that of the artist and the scientist, continue at any moment in all men:
the creation of self by self, the enlargement of personality by an effort that
takes much from little, something from nothing, and constantly adds to what was
rich in the world?

Vue du dehors, la nature apparaît comme une immense efflorescence
d’imprévisible nouveauté ; la force qui l’anime semble créer avec amour, pour
rien, pour le plaisir, la variété sans fin des espèces végétales et animales ;
à chacune elle confère la valeur absolue d’une grande œuvre d’art ; on dirait
qu’elle s’attache à la première venue autant qu’aux autres, autant qu’à
l’homme. Mais la forme d’un vivant, une fois dessinée, se répète indéfiniment ;
mais les actes de ce vivant, une fois accomplis, tendent à s’imiter eux-mêmes
et à se recommencer automatiquement: automatisme et répétition, qui dominent
partout ailleurs que chez l’homme, devraient nous avertir que nous sommes ici à
des haltes, et que le piétinement sur place, auquel nous avons affaire, n’est
pas le mouvement même de la vie. Le point de vue de l’artiste est donc
important, mais non pas définitif. La richesse et l’originalité des formes
marquent bien un épanouissement de la vie ; mais dans cet épanouissement, dont
la beauté signifie puissance, la vie manifeste aussi bien un arrêt de son élan
et une impuissance momentanée à pousser plus loin, comme l’enfant qui arrondit
en volte gracieuse la fin de sa glissade.

Seen from the outside, nature appears as an immense efflorescence of
unpredictable novelty; the force that animates it seems to create with love,
for nothing, for pleasure, the endless variety of plant and animal species; to
each one it confers the absolute value of a great work of art; it seems to be
attached to the first coming as much as to the others, as much as to man. But
the form of a living being, once drawn, repeats itself indefinitely; but the
acts of this living being, once accomplished, tend to imitate themselves and to
begin again automatically: automatism and repetition, which dominate everywhere
else than in man, should warn us that we are here at halts, and that the
trampling on the spot, with which we are dealing, is not the very movement of
life. The artist's point of view is therefore important, but not definitive.
The richness and the originality of the forms mark indeed a blossoming of life;
but in this blossoming, whose beauty means power, life manifests as well a stop
of its momentum and a momentary impotence to push further, like the child who
rounds in graceful volte the end of his slide.

Supérieur est le point de vue du moraliste. Chez l’homme seulement, chez les
meilleurs d’entre nous surtout, le mouvement vital se poursuit sans obstacle,
lançant à travers cette œuvre d’art qu’est le corps humain, et qu’il a créée au
passage, le courant indéfiniment créateur de la vie morale. L’homme, appelé
sans cesse à s’appuyer sur la totalité de son passé pour peser d’autant plus
puissamment sur l’avenir, est la grande réussite de la vie. Mais créateur par
excellence est celui dont l’action, intense elle-même, est capable
d’intensifier aussi l’action des autres hommes, et d’allumer, généreuse, des
foyers de générosité. Les grands hommes de bien, et plus particulièrement ceux
dont l’héroïsme inventif et simple a frayé à la vertu des voies nouvelles, sont
révélateurs de vérité métaphysique. Ils ont beau être au point culminant de
l’évolution, ils sont le plus près des origines et rendent sensible à nos yeux
l’impulsion qui vient du fond. Considérons-les attentivement, tâchons
d’éprouver sympathiquement ce qu’ils éprouvent, si nous voulons pénétrer par un
acte d’intuition jusqu’au principe même de la vie. Pour percer le mystère des
profondeurs, il faut parfois viser les cimes. Le feu qui est au centre de la
terre n’apparaît qu’au sommet des volcans.

Superior is the point of view of the moralist. In man alone, especially in the
best among us, the vital movement continues unimpeded, launching through this
work of art that is the human body, and which it has created in passing, the
indefinite creative current of moral life. Man, who is constantly called upon
to rely on the totality of his past in order to weigh all the more powerfully
on the future, is the great success of life. But the creator par excellence is
the one whose action, intense in itself, is capable of intensifying the action
of other men, and of igniting, generously, fires of generosity. The great men
of goodness, and more particularly those whose inventive and simple heroism has
opened up new paths, are revealing metaphysical truth. They may be at the
culmination of evolution, but they are closest to the origins and make us
sensitive to the impulse that comes from the bottom. Let us consider them
attentively, let us try to sympathetically experience what they feel, if we
want to penetrate by an act of intuition to the very principle of life itself.
To pierce the mystery of the depths, we must sometimes aim for the peaks. The
fire that is at the center of the earth only appears at the tops of volcanoes.
