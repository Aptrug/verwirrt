# Providence personnelle

## aus arasite.org/nietgaysci.html

En vieillissant, on se rend compte que la vie se déroule généralement pour le
mieux, et que même les revers apparents s'avèrent importants et significatifs
sur le plan personnel.  Cela nous aide à réaliser à quel point le dieu chrétien
est mesquin, avec son apparente omnipotence et son exigence de service constant
: Epicure a eu une meilleure idée.  Mais en fin de compte, ce sont nos propres
compétences en matière d'interprétation et d'action qui ont permis d'aboutir à
cet heureux résultat, même si nous ne nous en donnons guère le crédit.  Même un
heureux hasard ne peut pas faire mieux.
