# In media vita

## aus arasite.org/nietgaysci.html

Penser la vie comme une série d'expériences est libérateur, et les dangers qui
nous guettent nous encouragent à "trouver des endroits pour danser et jouer"
(255).  L'expérience de la vie mène à la connaissance
