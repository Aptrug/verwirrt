# Volonté et vague

## aus arasite.org/nietgaysci.html

Nous vivons comme les vagues de la mer, toujours en train d'avancer avec
impatience et en renouvelant constamment nos poussées.  Il ne sert à rien de se
méfier des vagues ou de les craindre [Kaufmann, 248 ans, dit que ce passage
poétique est le fruit d'un état d'esprit exubérant, savourant sa propre vitalité
débordante, même s'il s'est rendu compte qu'il était malade et à moitié aveugle.
La valeur du jeu revient constamment, même chez les enfants.  Il y a aussi une
notion de récurrence éternelle, où ce qui semble être une répétition sans fin
s'avère être libérateur et délicieux tant que l'on adopte une perspective
dionysiaque]
