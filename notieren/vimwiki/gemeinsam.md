# gemeinsam

## aus <http://pages.hmc.edu/beckman/philosophy/Nietzsche/reading/Gay.html>

Comme le note Kaufmann, la dédicace de ce livre à Sanctus Januarius
célèbre la guérison de Nietzsche après une longue période de dépression,
de solitude et de pensées sombres. À ce stade de l'écriture de
La science gaie, Nietzsche sent son sang couler à flot. Ainsi,
l'aphorisme d'ouverture est une déclaration puissante de la direction
qu'il se voit prendre. #Le n°276 annonce la célèbre étreinte de
Nietzsche de l'amor fati, ou "amour du destin", mais qu'est-ce
que cela signifie. En particulier, est-ce une sorte de déterminisme
? De toute évidence, il s'agit d'un point très différent. Pour
Nietzsche, le problème est le fait que toute la tendance de la
civilisation a été la construction de "contre-réalités", la négation
de la nécessité naturelle par l'affirmation de ces constructions.
Dans la naturalisation et la dé-définition de Nietzsche, il cherche
à accepter la nécessité naturelle comme belle, voire à pouvoir
lui dire "oui". Son argument contre le christianisme deviendra
de plus en plus clair comme une condamnation des manières dont
le christianisme a dit "non" à la vie. Cet esprit est repris dans
le n° 278, "La pensée de la mort", où Nietzsche conclut : "J'aimerais
beaucoup faire quelque chose qui rendrait la pensée de la vie cent
fois plus attrayante pour eux". (c'est moi qui souligne)

Au numéro 283, nous trouvons une déclaration extrêmement importante
du projet éthique de Nietzsche, et cela se poursuit au numéro 290.
Ce projet nous fera passer des quelques âmes nobles de son âge
à un âge d'"humains préparatoires" qui seront eux-mêmes remplacés
par une race de tels êtres. Il utilise même avec insistance le
mot "vaincre" dans ce contexte. Qu'y a-t-il en chacun de nous,
voire en toute chose, qui doit être surmonté ? Nietzsche utilise
continuellement un langage rhétorique visant l'héroïsme, la guerre,
la vie dangereuse, etc. Mais il est extrêmement important de reconnaître
que ce sont des guerres de l'esprit. Il est difficile de voir à
travers la croûte culturelle qui nous habille d'habitude ; nous
devons donc nous rendre à nous-mêmes prêts à faire la guerre et
prêts à prendre les risques de la pensée originale et du scepticisme.
Comme le thème se poursuit dans le n° 285, Nietzsche souligne la
constance de l'énergie nécessaire dans le type d'existence qu'il
propose et suggère que "vous aurez l'éternelle récurrence de la
guerre et de la paix". Et enfin, "peut-être que l'homme s'élèvera
toujours plus haut dès qu'il cessera de couler vers un dieu." Notez
au n°292 son hostilité envers la morale en tant que telle. Le projet
de Nietzsche est un système éthique qui doit être repris par des
individus courageux et aventureux, et non une formule à imposer
aux individus pour étouffer leur propre activité. Au numéro 301,
il souligne une fois encore le point crucial suivant : alors que
nous nous considérons toujours comme des "observateurs" de la vie,
je suppose que "la vie nous arrive", nous sommes en réalité les
poètes qui continuent à créer la vie. C'est particulièrement vrai
pour les valeurs. Les choses naturelles n'ont pas de valeur intrinsèque,
pour Nietzsche, car c'est nous qui leur donnons de la valeur. "Nous
sommes les seuls à avoir créé le monde qui concerne l'homme !"

Dans les numéros 293 et 300, le projet est à nouveau lié à la science,
comme le montre le titre de cet ouvrage, et le numéro 300 est particulièrement
révélateur de la relation entre l'essor de la science par la magie
et la nécessité d'une science homosexuelle issue d'une conquête
héroïque dans le dépassement. Nietzsche mentionne Prométhée, mais
il est pertinent de noter que les Amérindiens de l'Ouest des États-Unis
ont tous associé le savoir et la flexibilité éthique (parfois dans
une mesure scandaleuse) à des figures de tricheurs — Coyote dans
une grande partie de l'Ouest et Corbeau dans le Nord-Ouest —
qui étaient tous associés à la création. Ce thème culmine avec
le n°335, "Vive la physique !" Comme l'écrit Kaufmann dans sa note
de bas de page n°67, il faut se demander ce que Nietzsche entendait
par "physique". Pour rester cohérent avec ses premières discussions,
il ne peut pas signifier "physique théorique" ou science théorique
en général. Dans tout The Gay Science, il utilise systématiquement
des termes comme "observation" et "expérience", mais jamais "théorie".
Le genre de science que Nietzsche semble garder à l'esprit est
une recherche de la vérité audacieusement critique, soigneusement
observée et expérimentalement créative. Nietzsche ne limite pas
non plus cette recherche de la vérité aux seuls sujets classiques
des sciences naturelles du XIXe siècle. Comme le suggère Kaufmann,
il est beaucoup plus probable que Nietzsche signifie "physique"
au sens large du mot grec "Physique" d'Aristote, qui est une étude
de toutes les choses naturelles observables, y compris les êtres
humains et leurs attributs, comme la psychologie.

Les aphorismes finaux reprennent les thèmes recueillis dans The
Gay Science et jettent les bases de Zarathoustra, bien qu'il faille
reconnaître que le Livre I de Zarathoustra était une année entière
dans l'avenir et que Nietzsche a vécu un traumatisme extrême entre-temps.
Notez, au n° 339, sa déclaration selon laquelle "Oui, la vie est
une femme", et comparez cela dans un ouvrage ultérieur à "si la
vérité était une femme, que se passerait-il alors ? Je ne crois
pas que l'intention de l'un ou l'autre soit négative ; en effet,
il semble dire que la vie et la vérité sont de belles choses, bien
qu'elles ne soient pas toujours faciles à comprendre. Sa déception
concernant la scène de la mort de Socrate réside dans le fait que
Socrate continue de nier la vie en tant que valeur, et son introduction
de l'"Eternelle Récurrence du Même", au n° 341, représente une
étape importante dans la prise de la vie au sérieux. En effet,
en vivant la vie, nous devrions toujours demander : "Désirez-vous
cela une fois de plus et d'innombrables fois encore ? Et enfin,
la figure de Zarathoustra se présente comme le vrai bénéfice. Ailleurs,
Nietzsche observe qu'il a une grande admiration pour le Christ
mais qu'il est mort trop jeune et immature, ainsi qu'inexpérimenté.
Zarathoustra est mûr et expérimenté, tout en étant en contact avec
le monde naturel.

## aus <http://pages.hmc.edu/beckman/philosophy/Nietzsche/Gay.htm>

Le dernier aphorisme du livre IV, le n° 342, commence littéralement
le prochain livre de Nietzsche, Ainsi parlait Zarathoustra. L'atmosphère
du livre IV est entièrement un prélude à ce prochain ouvrage, c'est-à-dire
une succession de sages suggestions sur la façon de bien vivre.
Il dit "Oui" à la vie, il suggère de reconstruire les valeurs avec
audace et il adopte le point de vue selon lequel une vie saine
est douloureuse et difficile, mais louable en même temps. Le livre
IV est consacré au mythe de Saint Janvier, dont le sang coule à
nouveau chaque année, chaud et liquide. Sur le plan rhétorique,
une grande partie de ce livre prend la voix d'un "professeur".
En tout, il s'agit d'une élaboration puissamment soutenue du concept
de Nietzsche sur la science gay, c'est-à-dire d'une approche héroïque
et créative de la vie.

Le premier aphorisme, le n° 276, est tout aussi intrigant, sinon aussi
célèbre. Dans cet aphorisme, Nietzsche s'allie à l'esprit d'Amor
fati, l'amour du destin. Mais ce que Nietzsche entend par là, c'est
bien plus qu'une forme de fatalisme. Comme il le dit, "Je veux
apprendre de plus en plus à voir comme beau ce qui est nécessaire
dans les choses. ... un jour, je souhaite n'être qu'un béni-oui-oui".
Le but est de comprendre ce qui est et ce qui a été en toute honnêteté
et avec un réalisme naturel ; en outre, dans l'esprit de ce livre,
le but est de consentir, de saluer ce qui est nécessairement avec
joie. L'esprit de ce livre se poursuit au n° 278 où il observe
notre possession avec la pensée de la mort et suggère que la pensée
de la vie devrait prévaloir.

Le n°283 est extrêmement important en tant que prélude aux idées de
Nietzsche sur l'uebermensch, ou "surhomme", et à l'important processus
de "dépassement de soi". Nietzsche veut des personnes pleines d'entrain,
de courage et qui prendront plaisir à se battre. Les gens doivent
être aguerris et prêts à supporter la perte. Ils doivent être prêts
à se remettre en question, à trouver en eux-mêmes ce qui doit être
surmonté. Ils doivent vivre dangereusement. Tout cela est le plus
souvent assez mal compris, et cela a certainement été mal compris
par la soeur nazie de Nietzsche et par la direction ultérieure
du Troisième Reich. Ce qui intéresse Nietzsche, c'est le caractère
(valeurs, autodiscipline et esprit) du guerrier et non pas le fait
de faire littéralement la guerre. D'un point de vue psychologique,
nous devons voir qui est Nietzsche lui-même — un vagabond en
terre étrangère, survivant avec une petite pension, souffrant souvent
pendant des jours de maux de tête et de nausées atroces — et
pourtant, pour tout cela, il se sentait élevé et se lançait dans
une sorte de découverte de soi, voire du monde, qui dépassait de
loin ce que d'autres personnes avaient jamais pu réaliser. Tout
cela devrait être évident si l'on en croit sa description de cet
"âge supérieur" comme "l'âge qui amènera l'héroïsme dans la recherche
du savoir et qui fera la guerre pour le bien des idées et de leurs
conséquences". Ce thème est repris de façon spectaculaire dans
le numéro 288-9. Cette vie dangereuse n'est pas non plus une anarchie.
En #290, il suggère que l'anarchie vient de la faiblesse ; la grandeur
développe son propre style et prospère dans la discipline d'un
style. "Donner du style à son caractère, un art grand et rare !"
Mais même avec du style, la vie du guerrier/prophète est toujours
difficile ; tout ce thème est repris dans le n°316

L'aphorisme 301 est la prochaine étape la plus importante dans le développement
général de cet être supérieur. Alors que l'être supérieur est un
guerrier à la recherche de la connaissance et se considère donc
comme un contemplatif, il "oublie qu'il est lui-même le poète qui
continue à créer cette vie". L'homme supérieur n'est pas un simple
spectateur ; il fait partie de "ceux qui façonnent vraiment continuellement
quelque chose qui n'existait pas auparavant : l'ensemble du monde
éternellement croissant des évaluations, des couleurs, des accents,
des perspectives, des échelles, des affirmations et des négations".
Nous, les humains, sommes des donneurs de valeurs. L'aphorisme
se termine par l'une des plus puissantes déclarations de Nietzsche.
"Tout ce qui a de la valeur dans notre monde n'a plus de valeur
en soi, selon sa nature — la nature est toujours sans valeur,
mais on lui a donné de la valeur à un moment donné, en tant que
cadeau — et c'est nous qui l'avons donné et accordé. Nous sommes
les seuls à avoir créé le monde qui concerne l'homme !" Mais n'oublions
pas que le pouvoir créatif qui est en nous n'est pas une simple
anarchie de constructions. Nous créons la vie par la critique.
Dans l'aphorisme n° 307, il suggère que "lorsque nous critiquons
quelque chose, ce n'est pas un événement arbitraire et impersonnel
; c'est, au moins très souvent, la preuve que des énergies vitales
en nous grandissent et se dépouillent".

Les numéros 324, 327-329 et 335 culminent le développement de la "science
gay" avec "Vive la physique ! Et encore plus ce qui nous oblige
à nous tourner vers la physique — notre honnêteté !" La science,
la philosophie, quel que soit le nom que l'on veuille donner à
cette quête, n'a pas à être sobre, sérieuse, autoritaire et écrasante
pour l'ego. Il doit y avoir de la joie dans l'apprentissage, même
si nous apprenons à détruire de vieilles illusions, même si nous
apprenons par une discipline stricte, même si cela semble dangereux
(parce que nous avons peut-être sapé nos fondements mêmes).

En passant, il faut noter les numéros 333 et 334. Dans le premier,
Nietzsche préfigure puissamment Freud en suggérant que "la plus
grande partie de l'activité de notre esprit reste de loin inconsciente
et non ressentie". Dans le second aphorisme, nous voyons Nietzsche
dans une réflexion vraiment tendre et humaine — "l'amour, lui
aussi, doit être appris". "A la fin, nous sommes toujours récompensés
pour notre bonne volonté, notre patience, notre esprit d'ouverture
et notre douceur avec ce qui est étrange ; peu à peu, il perd son
voile et se révèle d'une beauté nouvelle et indescriptible." #Les
numéros 338-339 sont également dignes de mention, car ils marquent
le début de l'attaque soutenue de Nietzsche contre ce qu'il appelle
la "religion de la pitié".

À la toute fin du livre IV et, par conséquent, à la fin originale
de La science gay, nous trouvons l'aphorisme n° 341, "Le plus grand
poids". Au cours de l'été précédent (août 1881), Nietzsche avait
vécu cette révélation ; dans son Ecce Homo, il la considérait comme
son idée la plus profonde — "cette formule d'affirmation la plus
élevée qui soit atteignable". C'est le concept de "récurrence éternelle
de la même chose". Il l'avait déjà brièvement suggéré dans le numéro 285.
Que ferions-nous si nous savions que notre destin est de revivre
les mêmes moments, sans fin, à travers l'éternité ? Ne serait-ce
pas un grand poids et un chagrin impossible ? Mais pouvons-nous
imaginer l'esprit d'une personne qui a tellement accepté la vie
et qui ressent une telle joie dans l'instant que ce serait un destin
bienvenu ? Ce n'est pas que Nietzsche pense que tout cela est vraiment
plausible ; c'est l'idée de ce que cela signifierait pour nous
et la discipline que cela nous imposerait. Imaginez que nous nous
engagions tellement dans les décisions que nous prenons que nous
accepterions volontiers - non, joyeusement - leur éternel retour
parmi nous. Ne devrions-nous pas prendre la vie beaucoup plus au
sérieux — plus honnêtement — que la plupart d'entre nous ne
le font maintenant ?
