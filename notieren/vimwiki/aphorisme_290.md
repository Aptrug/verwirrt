# Une chose est nécessaire

## aus <https://www.reddit.com/r/Nietzsche/comments/gf5zm5/the_gay_science_nihilism_and_living_as_artists/>

Donner du style à son personnage est un art grand et rare. Comme
un artiste moulant de l'argile, nous pouvons nous modeler à partir
des matériaux que nous sommes : nos talents naturels, nos faiblesses,
nos instincts, nos passions, nos inclinations, etc. Nous pouvons
recenser les forces et les faiblesses de notre nature et, par la
pratique quotidienne, les intégrer dans un plan artistique en mettant
l'accent sur certains aspects de nous-mêmes et en en cachant d'autres.
Il est important de noter que ce grand et rare art utilise un seul
goût - sa propre vision esthétique. Les personnes au caractère
fort aiment adhérer à leur style comme à une loi qu'ils se sont
eux-mêmes donnée ; les personnes au caractère faible détestent
la contrainte d'un style - elles préfèrent interpréter leur environnement
comme sauvage, arbitraire, désordonné et surprenant. Que l'on ait
un esprit fort ou faible, une chose est nécessaire : l'homme doit
atteindre la satisfaction de soi, car ce n'est qu'à cette condition
qu'il est tolérable. Celui qui n'est pas satisfait de lui-même
est prêt à se venger - il va déverser son ressentiment sur les
autres et nous serons ses victimes.

## aus arasite.org/nietgaysci.html

Beaucoup de gens font de leurs propres forces et faiblesses un style personnel,
réinterprétant la laideur et remettant à plus tard le jugement.  Cependant, les
forts et les dominateurs sont capables d'imposer leur goût et d'atteindre la
gaieté en dominant la nature avec leur style personnel.  Les faibles détestent
cependant le style et préfèrent se considérer comme encourageant la nature
libre, "sauvage, arbitraire, fantastique, désordonnée et surprenante" (233).
C'est au moins une façon de se satisfaire d'eux-mêmes et de freiner leur désir
de vengeance, et cela les empêche de parader leur laideur
