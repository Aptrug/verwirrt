# La réputation de fermeté

## aus <https://www.reddit.com/r/Nietzsche/comments/gh1g9t/the_gay_science_life_as_an_experiment_1316/>

La société veut que nous ayons une réputation parce qu'une réputation
indique un caractère prévisible, ce qui leur est utile (voir n°
21). Mais un caractère ferme est contraire à la poursuite de la
connaissance, qui exige d'expérimenter et d'être suffisamment flexible
pour aller à l'encontre de nos opinions et valeurs antérieures.
Il est difficile de vivre la vie comme une expérience lorsque l'on
ressent l'opposition entre des opinions pétrifiées et fermes qui
sont monopolisées.

## aus arasite.org/nietgaysci.html

Vous obtenez une réputation en étant cohérent et en vous conformant à la morale
des sociétés, mais cela est très mauvais pour la pensée critique, ou pour
quiconque change d'opinion.  C'est particulièrement difficile si vous connaissez
toutes sortes de bonnes manières de se comporter dans le passé.
