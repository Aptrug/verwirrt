# Les Contemplations

## Pauca Meae

### Global

<https://quizlet.com/ch/367754412/pauca-meae-resume-par-poeme-flash-cards/>
<https://prezi.com/p/makcq2kzd-kv/devoirs-victor-hugo/>
<https://www.etudier.com/dissertations/Pauca-Me%C3%A6/81456049.html>
<http://seassauduby.free.fr/hugo_contemplations_livre4.pdf>
<https://coggle.it/diagram/WNvaa41pGQAB51MI/t/trois-ans-apr%C3%A8s-victor-hugo-lecture-analytique>

### Par Poème

#### [Pure_innocence](Pure_innocence)

#### [15_fevrier_1843_et_4_septembre_1843](15_fevrier_1843_et_4_septembre_1843)

#### [Trois_ans_apres](Trois_ans_apres)

#### [Oh_je_fus_comme_fou](Oh_je_fus_comme_fou)

#### [Elle_avait_pris_ce_pli](Elle_avait_pris_ce_pli)

#### [Quand_nous_habitions_tous_ensemble](Quand_nous_habitions_tous_ensemble)

#### [Elle_etait_pale](Elle_etait_pale)

#### [A_qui_donc_sommes nous](A_qui_donc_sommes-nous)

#### [O_souvenirs](O_souvenirs)

#### [Pendant_que_le_marin](Pendant_que_le_marin)

#### [On_vit_on_parle_on_a_le_ciel_et_les_nuages](On_vit_on_parle_on_a_le_ciel_et_les_nuages)

#### [A_quoi_songeaient_les_deux_cavaliers_dans_la_foret](A_quoi_songeaient_les_deux_cavaliers_dans_la_foret)

#### [Veni_vidi_vixi](Veni_vidi_vixi)

#### [Demain_des_laube](Demain_des_laube)

#### [A_Villequier](A_Villequier)

#### [Mors](Mors)

#### [Charles_Vacquerie](Charles_Vacquerie)

### En marche

#### [A_Aug_V](A_Aug_V)

#### [Au_fils_dun_poete](Au_fils_dun_poete)

#### [Ecrit_en_1846_Ecrit_en_1855](Ecrit_en_1846_Ecrit_en_1855)

#### [La_source_tombait_du_rocher](La_source_tombait_du_rocher)

#### [A_Mademoiselle_Louise_B](A_Mademoiselle_Louise_B)

#### [A_vous_qui_etes_la](A_vous_qui_etes_la)

#### [Pour_lerreur_eclairer_cest_apostasier](Pour_lerreur_eclairer_cest_apostasier)

#### [A_Jules_J](A_Jules_J)

#### [Le_mendiant](Le_mendiant)

#### [Aux_feuillantines](Aux_feuillantines)

#### [Ponto](Ponto)

#### [Dolorosae](Dolorosae)

#### [Paroles_sur_la_dune](Paroles_sur_la_dune)

#### [Claire_P](Claire_P)

#### [A_Alexandre_D](A_Alexandre_D)

#### [Lueur_au_couchant](Lueur_au_couchant)

#### [Mugitusque_boum](Mugitusque_boum)

#### [Apparition](Apparition)

#### [Au_poete_qui_menvoie_une_plume_daigle](Au_poete_qui_menvoie_une_plume_daigle)

#### [Cerigo](Cerigo)

#### [A_Paul_M](A_Paul_M)

#### [Je_payai_le_pecheur_qui_passa_son_chemin](Je_payai_le_pecheur_qui_passa_son_chemin)

#### [Pasteurs_et_troupeaux](Pasteurs_et_troupeaux)

#### [Jai_cueilli_cette_fleur_pour_toi](Jai_cueilli_cette_fleur_pour_toi)

#### [O_strophe_du_poete_autrefois](O_strophe_du_poete_autrefois)

#### [Les_malheureux](Les_malheureux)
