n = int(input('Donner n: '))
C = [[1 for i in range(n + 1)] for j in range(n + 1)]
for i in range(2, n + 1):
    for j in range(1, i):
        C[i][j] = C[i - 1][j] + C[i - 1][j - 1]
print('Triangle: ')
for i in range(n + 1):
    print('n = ', i, end='\t\t')
    for j in range(i + 1):
        print(C[i][j], end='\t')
    print()
