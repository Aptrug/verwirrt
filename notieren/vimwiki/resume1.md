# Résume 1

## texte

Les journaux parlaient souvent d'une dette qui était due à la société. Il
fallait, selon eux, la payer. Mais cela ne parle pas à l'imagination. Ce qui
comptait, c'était une possibilité d'évasion, un saut hors du rite implacable,
une course à la folie qui offrit toutes les chances de l'espoir. Naturellement,
l'espoir, c'était d'être abattu au coin d'une rue, en pleine course, et d'une
balle à la volée. Mais, tout bien considéré, rien ne me permettait ce luxe, tout
me l'interdisait, la mécanique me reprenait.

Malgré ma bonne volonté, je ne pouvais pas accepter cette certitude insolente.
Car enfin, il y avait une disproportion ridicule entre le jugement qui l'avait
fondée et son déroulement imperturbable à partir du moment où ce jugement avait
été prononcé. Le fait que la sentence avait été lue à vingt heures plutôt qu'à
dix-sept, le fait qu'elle aurait pu être tout autre, qu'elle avait été prise par
des hommes qui changent de linge, qu'elle avait été portée au crédit d'une
notion aussi imprécise que le peuple français (ou allemand, ou chinois), il me
semblait bien que tout cela enlevait beaucoup de sérieux à une telle décision.
Pourtant, j'étais obligé de reconnaître que dès la seconde où elle avait été
prise, ses effets devenaient aussi certains, aussi sérieux, que la présence de
ce mur tout le long duquel j'écrasais mon corps.

Je me suis souvenu dans ces moments d'une histoire que maman me racontait à
propos de mon père. Je ne l'avais pas connu. Tout ce que je connaissais de
précis sur cet homme, c'était peut-être ce que m'en disait alors maman : il
était allé voir exécuter un assassin. Il était malade à l'idée d'y aller. Il
l'avait fait cependant et au retour il avait vomi une partie de la matinée. Mon
père me dégoûtait un peu alors. Maintenant, je comprenais, c'était si naturel.
Comment n'avais-je pas vu que rien n'était plus important qu'une exécution
capitale et que, en somme, c'était la seule chose vraiment intéressante pour un
homme ! Si jamais je sortais de cette prison, j'irais voir toutes les exécutions
capitales. J'avais tort, je crois, de penser à cette possibilité. Car à l'idée
de me voir libre par un petit matin derrière un cordon d'agents, de l'autre côté
en quelque sorte, à l'idée d'être le spectateur qui vient voir et qui pourra
vomir après, un flot de joie empoisonnée me montait au cœur. Mais ce n'était pas
raisonnable. J'avais tort de me laisser aller à ces suppositions parce que,
l'instant d'après, j'avais si affreusement froid que je me recroquevillais sous
ma couverture. le claquais des dents sans pouvoir me retenir.

Mais, naturellement, on ne peut pas être toujours raisonnable. D’autres fois,
par exemple, je faisais des projets de loi. Je réformais les pénalités. J’avais
remarqué que l’essentiel était de donner une chance au condamné. Une seule sur
mille, cela suffisait pour arranger bien des choses. Ainsi, il me semblait qu’on
pouvait trouver une combinaison chimique dont l’absorption tuerait le patient
(je pensais : le patient) neuf fois sur dix. Lui le saurait, c’était la
condition. Car en réfléchissant bien, en considérant les choses avec calme, je
constatais que ce qui était défectueux avec le couperet, c’est qu’il n’y avait
aucune chance, absolument aucune. Une fois pour toutes, en somme, la mort du
patient avait été décidée. C’était une affaire classée, une combinaison bien
arrêtée, un accord entendu et sur lequel il n’était pas question de revenir. Si
le coup ratait, par extraordinaire, on recommençait. Par suite, ce qu’il y avait
d’ennuyeux, c’est qu’il fallait que le condamné souhaitât le bon fonctionnement
de la machine. Je dis que c’est le côté défectueux. Cela est vrai, dans un sens.
Mais, dans un autre sens, j’étais obligé de reconnaître que tout le secret d’une
bonne organisation était là. En somme, le condamné était obligé de collaborer
moralement. C’était son intérêt que tout marchât sans accroc.

## traduit

Newspapers often talked about a debt that was owed to society. They said it had
to be paid. But that doesn't speak to the imagination. What counted was the
possibility of escape, a leap out of the implacable rite, a race to madness that
offered every chance of hope. Naturally, hope was to be shot on the corner of a
street, in the middle of a race, and to be shot on the fly. But, all things
considered, nothing allowed me this luxury, everything forbade it, the mechanics
took me back.

Despite my good will, I could not accept this insolent certainty. For, finally,
there was a ridiculous disproportion between the judgment on which it was based
and its imperturbable course from the moment it was pronounced. The fact that
the sentence had been read at eight o'clock rather than at five o'clock, the
fact that it could have been quite different, that it had been taken by men
changing laundry, that it had been credited to a notion as imprecise as the
French (or German or Chinese) people, it seemed to me that all this took away a
great deal of seriousness from such a decision. Yet I was obliged to recognise
that from the second it was taken, its effects became as certain, as serious, as
the presence of that wall along which I was crushing my body.

In those moments I remembered a story my mother used to tell me about my father.
I didn't know him. Maybe all I knew about him was what Mum was telling me at the
time: he had gone to see a murderer executed. He was sick at the thought of
going there. He did go, however, and when he came back he had vomited for part
of the morning. My father disgusted me a bit then. Now I understood, it was so
natural. How could I not see that nothing was more important than a capital
execution and that, in short, it was the only thing that was really interesting
for a man! If I ever got out of that prison, I would go and see all the capital
executions. I was wrong, I think, to think about that possibility. Because at
the idea of seeing myself free one morning behind a cordon of officers, on the
other side in a way, at the idea of being the spectator who comes to see and who
can vomit afterwards, a flood of poisonous joy rose in my heart. But it wasn't
reasonable. I was wrong to let myself go with these suppositions because the
next moment I was so terribly cold that I cowered under my blanket, snapping my
teeth and not being able to hold back.

But, of course, one cannot always be reasonable. Other times, for example, I was
making bills. I was reforming penalties. I had noticed that the main thing was
to give the convict a chance. Just one in a thousand was enough to make a lot of
things right. So it seemed to me that you could find a chemical combination
whose absorption would kill the patient (I thought: the patient) nine times out
of ten. He would know, that was the condition. For thinking about it calmly,
calmly considering things, I realised that what was wrong with the cleaver was
that there was no chance, absolutely no chance at all. Once and for all, in
short, the patient's death had been decided. It was a closed case, a settled
combination, an agreement that had been heard and there was no question of going
back on it. If it failed, extraordinarily, we would do it again. The annoying
thing was that the condemned man had to want the machine to work properly. I say
it's the defective side. This is true, in a way. But, in another sense, I was
obliged to recognise that the secret of good organisation was there. In short,
the condemned man was obliged to collaborate morally. It was in his interest
that everything went smoothly.

## le résultat

Ils disent qu'un crime doit être puni. Même si
c'était impossible, j'ai toujours pensé à m'échapper.
Attendre l'heure de l'exécution, c'est la première
partie de la punition, l'autre est sur l'échafaud,
où je préfèrerais n'être qu'un spectateur, ce qui
reste juste un vœu pieu.

nombre de mots: 55
