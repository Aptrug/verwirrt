# Disziplin

## 23. Januar 2021

Ich sollte aufhören. Es werden Jahre aus meinem Leben verbraucht. Zeit mit sinnlosem Zeug vergeuden.
Es bringt mein Gehirn und meinen Körper durcheinander. Ein weiser Mann sagte einmal, dass Disziplin
alles ist; ich bin mir dessen bewusst, dennoch ließ ich mich immer wieder in genau dieselbe Falle
tappen. Ich habe mich einfach nicht darum gekümmert, habe mich nicht um die Verantwortung gekümmert.
Es muss nicht so sein. Man sagt, es ist nie zu spät, ich sage nein, manchmal ist es tatsächlich zu
spät. Soll ich dann meinem niedrigsten Instinkt nachgeben? Nein; es wurde genug Schaden angerichtet
und ich werde tun, was nötig ist, damit es nicht wieder passiert. Manchmal denke ich darüber nach,
ob es sich wirklich lohnt, diese schlechten Angewohnheiten aufzugeben, wohl wissend, dass es fast
jeder tut. Und wenn ich darüber nachdenke, wird mir klar, dass fast jeder eigentlich ein Verlierer
ist; ich bin nicht wie sie, ich werde nicht wie sie sein.

## 25. Januar 2021

Ich habe es wieder getan. Was habe ich mir nur dabei gedacht?
