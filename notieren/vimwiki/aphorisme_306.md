# Stoïciens et épicuriens

## aus arasite.org/nietgaysci.html

Les épicuriens choisissent des vies qui conviennent à leur irritabilité et à
leur intellectualisme.  Les stoïciens, en revanche, s'entraînent à devenir
indifférents à tout accident du destin, et aiment à manifester cette
indifférence.  Le stoïcisme est bon pour ceux qui sont vulnérables aux actions
des autres, mais ceux qui sont relativement sûrs d'eux font mieux de devenir
épicuriens
