# Mon chien

## aus arasite.org/nietgaysci.html

(Il pense à sa douleur comme à un chien, il ne veut pas penser à quelque chose
de méchant, il veut avoir son lion et son aigle pour lui rappeler combien il est
vraiment grand.  Il pense qu'il va soit se faire exploser, soit s'épuiser - son
vrai destin était pire, dit Kaufmann, pour être préservé comme une sorte de
célébrité morte-vivante par sa soeur]
