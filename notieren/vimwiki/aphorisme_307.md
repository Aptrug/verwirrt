# En faveur de la critique

## aus arasite.org/nietgaysci.html

Faire des erreurs est bon pour vous en fin de compte, si cela vous permet de
mener une vie plus pleine.  Se sentir critique à propos de quelque chose est
souvent un signe qu'il faut aller de l'avant.
