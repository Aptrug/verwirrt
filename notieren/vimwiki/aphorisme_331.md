# Plutôt sourd qu’assourdi

## aus arasite.org/nietgaysci.html

Pour acquérir une certaine notoriété sur un marché gigantesque, il faut
constamment "crier".  Pour bien réfléchir, il faut faire la sourde oreille aux
cris.
