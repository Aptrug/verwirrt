# Illusion des contemplatifs

## aus <https://www.reddit.com/r/Nietzsche/comments/gf5zm5/the_gay_science_nihilism_and_living_as_artists/>

Les humains voient et entendent plus que les autres animaux, et
nous le faisons de manière plus réfléchie ; les humains supérieurs
le font encore plus que la plupart des autres, et par conséquent
ils sont plus heureux, mais aussi plus malheureux parce qu'ils
se voient à tort comme des spectateurs et des auditeurs de la vie.
Ce type de personne supérieur n'est pas seulement un spectateur,
ni un acteur (des personnes qui se passent constamment de réflexion
ou de contemplation) ; le type de personne supérieur est un poète
doté d'un pouvoir créatif qui crée la vie qu'il vit. Nietzsche
se considère comme un type supérieur lorsqu'il dit : nous qui pensons
et ressentons en même temps, nous créons quelque chose qui n'existait
pas auparavant. Il ajoute : "Tout ce qui a de la valeur dans notre
monde aujourd'hui n'a pas de valeur en soi, selon sa nature - la
nature est toujours sans valeur, mais on lui a donné de la valeur
à un moment donné, en tant que présent - et c'est nous qui l'avons
donné et accordé. Nous sommes les seuls à avoir créé un monde qui
concerne l'homme ! En d'autres termes, le monde n'a pas de valeur
intrinsèque et il nous est indifférent ; toutes les valeurs viennent
de nous, les humains. Les types supérieurs ont tendance à oublier
qu'ils ont cette capacité, et donc à se sous-estimer.

## aus arasite.org/nietgaysci.html

Les êtres humains supérieurs sont beaucoup plus perceptifs et font plus de ce
qu'ils voient et entendent.  Il éprouve des plaisirs plus grands.  Le problème
est que la haute culture a fait de vous un spectateur, quelqu'un qui ne peut que
contempler, et non l'acteur du drame.  Le simple activisme n'a pas le sens du
pouvoir créatif.  Ceux qui peuvent penser et ressentir en même temps peuvent
vraiment créer quelque chose qui n'existait pas auparavant, et ajouter au monde
des couleurs, des perspectives, des affirmations et des négations, comme une
sorte de poème.  Les choses les plus précieuses n'ont pas de valeur en
elles-mêmes, parce que rien n'a de valeur dans la nature.  La valeur doit avoir
été ajoutée par les êtres humains, bien que nous l'oubliions souvent et que nous
ne soyons donc "ni aussi fiers ni aussi heureux que nous pourrions l'être"
(242).
