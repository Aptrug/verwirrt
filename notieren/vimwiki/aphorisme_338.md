# La volonté de souffrir et les compatissants

## aus arasite.org/nietgaysci.html

Notre souffrance personnelle est incompréhensible pour les autres, et lorsqu'ils
s'en aperçoivent, ils n'offrent que des interprétations superficielles.  Cela
découle du développement de la pitié, qui prend toujours une forme non
personnelle.  La pitié nous diminue.  Ceux qui ont pitié de nous supposent
souvent qu'ils peuvent détecter le rôle du destin, de sorte qu'ils n'ont pas
besoin de retracer les séquences personnelles particulières qui ont conduit à la
détresse.  Les personnes qui ont pitié souhaitent simplement aider, sans voir
comment la souffrance peut même être nécessaire.  Ils cherchent des solutions
rapides.  En fin de compte, la pitié dépend du plaisir d'être à l'aise.  Tenter
d'agir moralement ou avec pitié envers les autres peut amener les gens à perdre
leur propre chemin.  Il est facile de le faire par sympathie pour la souffrance,
mais c'est vraiment une façon séduisante d'échapper à la nécessité de vivre à
notre façon, avec notre propre conscience.  Beaucoup de gens sont attirés par
"le beau temple de la "religion de la pitié""(270).  Il en va de même pour ceux
qui sont enthousiasmés par les guerres nationalistes : la guerre peut offrir un
détour vers le suicide, mais "avec une bonne conscience".  Il peut être
nécessaire de vivre dans l'isolement si nous voulons vivre pour nous-mêmes, en
ignorant ce qui semble important pour les autres, en tenant le présent à
distance.  Nous ne devrions aider les autres qu'une fois que nous avons compris
leur détresse, et même alors, ne les aider qu'en les rendant plus audacieux,
plus aptes à persévérer, plus gais.  Nous devons partager non pas la souffrance,
mais la joie.
