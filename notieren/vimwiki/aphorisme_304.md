# En faisant, nous ne faisons pas

## aus <https://www.reddit.com/r/Nietzsche/comments/freubj/the_gay_science_critique_of_moralists_116/>

Nietzsche n'aime pas les morales qui disent à une personne de surmonter
ou de renoncer à son moi naturel (pour supprimer ses passions,
ses désirs, etc.) - mais en nous consacrant à ce que nous voulons
/ ce qui est bon pour nous, nous renonçons ainsi à d'autres choses,
qui peuvent être les mêmes que celles auxquelles d'autres nous
disent de renoncer (par exemple, une morale peut dire à une personne
d'éviter la drogue, mais ce n'est pas l'approche que soutient Nietzsche
; il préfère qu'une personne poursuive un certain objectif au point
de ne pas avoir de temps ou d'intérêt pour la drogue).

## aus arasite.org/nietgaysci.html

Il est acceptable d'être obsédé par un but moraliste, même si cela signifie
renoncer à d'autres choses.  La seule exception est si cela conduit à la
négativité et au déni de soi.
