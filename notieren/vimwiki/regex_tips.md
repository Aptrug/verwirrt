# regex tips

## Swap the first word with the last word in each line in file with Perl

perl -ape '($F[0],$F[-1])=($F[-1],$F[0]);$_="@F\n"' file

## Search for `Subscribe` but not `Subscribed`

Subscribe(?!.*d)

## vim

### match word between `""` followed by `:`

".\{-}":

### make the last word the first word

`:%s/^\(.\+\)\s\+\(\S\+\)$/\2 \1/`
