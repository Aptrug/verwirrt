# Socrate mourant

## aus <https://www.reddit.com/r/Nietzsche/comments/gqjm37/the_gay_science_the_value_of_life_1516/>

Nietzsche a des sentiments mitigés à l'égard de Socrates et ce
passage indique une grande partie des éloges et des critiques qu'il
a pour Socrates Nietzsche critique Socrates dans ce passage comme
étant un "pessimiste" (par lequel il entend quelqu'un qui ne pense
pas que la vie vaut la peine d'être vécue). La critique de Nietzsche
est basée sur les derniers mots de Socrates, selon le dialogue
que Platon a écrit et qui s'intitule "Criton". Socrate a été condamné
pour corruption de la jeunesse et impiété envers les dieux ; sa
punition devait être soit le bannissement d'Athènes, soit la mort — Socrate
a choisi la mort, mais ce n'est pas la question en soi.
Dans ses derniers moments avec son ami Criton, Socrate dit : "Oh
Criton, je dois un coq à Asclépios". Comme Asclépios était le dieu
de la médecine, Nietzsche interprète Socrate en disant qu'il doit
offrir un coq à Asclépios en guise de paiement parce que sa mort
sera un remède pour la vie. Plus généralement, la critique de Nietzsche
se fonde sur les conceptions de la vie et de la mort de Socrate
tirées des dialogues que Platon a écrits - Socrate s'attend à vivre
au-delà de la mort, où il compte également poursuivre la vérité
sans les limites du corps physique (une conception qui n'est pas
différente de l'espoir chrétien d'atteindre une meilleure connaissance
de Dieu et de la réalité à la mort). Et, étant donné la position
de ce passage juste avant le suivant, il semble que Socrate échouerait
au test de Nietzsche sur la récurrence éternelle (plus de détails
à ce sujet dans le prochain passage).

## aus arasite.org/nietgaysci.html

Nous devons admirer le courage et la sagesse de Socrate, qui savait non
seulement parler mais aussi se taire.  Il aurait dû se taire surtout lorsqu'il
était mourant, car nous pouvons interpréter ses dernières remarques comme disant
que la vie est une maladie.  Dans la pratique, il était d'une gaieté implacable,
et n'avait pas besoin de se livrer à un jugement définitif [Dans La naissance de
la tragédie, N n'aime pas Socrate, mais Kaufmann dit que cela est trompeur, et
qu'il y a plusieurs autres passages d'admiration].
