# Danger du plus heureux

## aus arasite.org/nietgaysci.html

Ceux qui ont des sens raffinés ont apprécié la vie et sont prêts à la risquer
dans l'intérêt de nouvelles découvertes.  Ils peuvent profiter de l'instant
présent, comme Homère l'a fait.  Cependant, ils peuvent aussi connaître un plus
grand malheur et une plus grande souffrance, de sorte que la moindre déception
peut gâcher la vie, comme cela est d'ailleurs arrivé à Homère lui-même [qui
semble avoir eu un manque d'humour et de bon sens].
