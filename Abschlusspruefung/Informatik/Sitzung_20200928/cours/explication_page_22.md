# Page 22

soit $f(n)$ le nombre d'opérations élémentaires

$$
f(n) = 2 + n^2 + 1 + n(n(1 + 3n + 1)) + 1 = O(n^3)
$$
